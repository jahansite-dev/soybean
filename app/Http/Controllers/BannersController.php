<?php

namespace App\Http\Controllers;

use App\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\View\View;

class BannersController extends Controller
{
    //add banner
    public function addBanner(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
//            echo "<pre>"; print_r($data);die;
            $banner = new Banner();
            $banner->title = $data['title'];
            $banner->link = $data['link'];

            if (!empty($data['status'])) {
                $status = '0';
            } else {
                $status = '1';
            }
            //upload image
            if ($request->hasfile('image')) {
                $image_tmp = $request->file('image');
                if ($image_tmp->isValid()) {
                    //upload image after resize
                    $extension = $image_tmp->extension();
                    $fileName = rand(111, 99999) . '.' . $extension;
                    $banner_path = public_path() . '/images/frontend_images/Banners';
                    //Image::make($banner_path)->resize(1665,375)->save($banner_path);
                    $image_tmp->move($banner_path, $fileName);
                    $banner->image = $fileName;
                }
            }
            $banner->status = $status;
            $banner->save();
            return redirect()->back()->with('flash_message_success', 'تصویر با موفقیت اضافه شد');
        }

        return view('admin.add_banner');
    }
    //edit banner
    public function editBanner(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
//            echo "<pre>"; print_r($data);die;

            if (!empty($data['status'])) {
                $status = '0';
            } else {
                $status = '1';
            }
            if (empty($data['title'])) {
                $data['title'] = '';
            }
            if (empty($data['link'])) {
                $data['link'] = '';
            }
            //upload image
            if ($request->hasfile('image')) {
                $image_tmp = $request->file('image');
                if ($image_tmp->isValid()) {
                    //upload image after resize
                    $extension = $image_tmp->extension();
                    $fileName = rand(111, 99999) . '.' . $extension;
                    $banner_path = public_path() . '/images/frontend_images/Banners';
                    //Image::make($banner_path)->resize(1665,375)->save($banner_path);
                    $image_tmp->move($banner_path, $fileName);
//                    $banner->image = $fileName;
                }
            } else if (!empty($data['current_image'])) {
                $fileName = $data['current_image'];
            } else {
                $fileName = '';
            }
            Banner::where('id', $id)->update(['status' => $status, 'title' => $data['title'], 'link' => $data['link'],'image'=>$fileName]);
            return redirect()->back()->with('flash_message_success','اسلایدر با موفقیت ویرایش شد');
        }
        $bannerDetails = Banner::where('id', $id)->first();
        return view('admin.edit_banners', compact('bannerDetails'));
    }
    //delete banner
    public function deleteBanner($id = null){
        Banner::where(['id'=>$id])->delete();
        return redirect()->back()->with('flash_message_success', 'اسلایدر با موفقیت حذف شد');
    }
    //view banner
    public function viewBanners()
    {
        $banners = banner::get();
        return view('admin.view_banners')->with(compact('banners'));
    }
}
