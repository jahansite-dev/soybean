<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function login(request $request){
        if($request->isMethod('post')){
            $data = $request->input();
            if(auth::attempt(['email'=>$data['email'],'password'=>$data['password'],'admin'=>'1'])){
                session::put('adminSession',$data['email']);
                return redirect('admin/dashboard');
            }else{
                return redirect('/admin')->with('flash_message_error','نام کاربری یا گذرواژه اشتباه است');
            }
        }
        return view('admin.admin_login');
    }
    public function dashboard(){
        if(Session::has('adminSession')){
            //perform all dashboard tasks
        }else{
            return redirect('/admin')->with('flash_message_error', 'لطفا برای دسترسی وارد شوید');
        }
        return view('admin/dashboard');
    }
    public function settings(){
        if(Session::has('adminSession')){
            //perform all dashboard tasks
        }else{
            return redirect('/admin')->with('flash_message_error', 'لطفا برای دسترسی وارد شوید');
        }
        return view('admin/settings');
    }
    public function  chkpassword(request $request){
        $data = $request->all();
        $current_password = $data['current_pwd'];
        $check_password = User::where(['admin'=>'1'])->first();
        if (hash::check($current_password, $check_password->password)){
            echo "true"; die;
        }else{
            echo "false"; die;
        }
    }
    public function logout(){
        session::flush();
        return redirect('/admin')->with('flash_message_success','با موفقیت خارج شدید');
    }
}