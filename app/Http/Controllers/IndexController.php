<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;
use App\Banner;

class IndexController extends Controller
{
    public function index(){
        $products = new ProductsController();
        return view('index', ['products' => $products]);
}
}
