<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageSeosTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'page_seo', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->integer('page_id')->unsigned()->index()->foreign()->references( "id" )->on( "pages" )->onDelete( "cascade" );
			$table->enum('type', ['video', 'page']);
			$table->string( 'title' );
			$table->text( 'tags' );
			$table->text( 'description' );
			$table->timestamps();
		} );
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop( 'page_seo' );
	}
}
