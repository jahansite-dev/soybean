<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_categories', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('page_id')->unsigned()->index()->foreign()->references( "id" )->on( "page" )->onDelete( "cascade" );
	        $table->integer('category_id')->unsigned()->index()->foreign()->references( "id" )->on( "categories" )->onDelete( "cascade" );;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_categories');
    }
}
