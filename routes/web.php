<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::match(['get','post'],'admin','AdminController@login');
Route::get('/admin/dashboard','AdminController@dashboard');
Route::get('/admin/check-pwd','AdminController@chkpassword');
Route::get('/admin/settings','AdminController@settings');
Route::match(['get','post'],'admin/update-pwd','AdminController@updatepassword');
Route::get('/logout','AdminController@logout');
Route::match(['get','post'],'/admin/add-banner','BannersController@addBanner');
Route::get('admin/view-banners','BannersController@viewBanners');
Route::match(['get','post'],'/admin/edit-banner/{id}','BannersController@editBanner');
Route::get('/admin/delete-banner/{id}','BannersController@deleteBanner');
Route::get('/','IndexController@index');
Route::match(['get', 'post'], '/admin/add-category','CategoryController@addCategory');
Route::match(['get', 'post'], '/admin/edit-category/{id}','CategoryController@editCategory');
Route::match(['get', 'post'], '/admin/delete-category/{id}','CategoryController@deleteCategory');
Route::get('/admin/view-categories','CategoryController@viewCategories');
Route::match(['get','post'],'/admin/add-product','ProductsController@addProduct');
Route::match(['get','post'],'/admin/edit-product/{id}','ProductsController@editProduct');
Route::get('/admin/delete-product/{id}','ProductsController@deleteProduct');
Route::get('/admin/view-products','ProductsController@viewProducts');
Route::get('/admin/delete-product-image/{id}','ProductsController@deleteProductImage');
Route::match(['get', 'post'], '/admin/add-images/{id}','ProductsController@addImages');
Route::get('/admin/delete-alt-image/{id}','ProductsController@deleteProductAltImage');
// Admin Attributes Routes
Route::match(['get', 'post'], '/admin/add-attributes/{id}','ProductsController@addAttributes');
Route::match(['get', 'post'], '/admin/edit-attributes/{id}','ProductsController@editAttributes');
Route::get('/admin/delete-attribute/{id}','ProductsController@deleteAttribute');
// Admin Coupon Routes
Route::match(['get','post'],'/admin/add-coupon','CouponsController@addCoupon');
Route::match(['get','post'],'/admin/edit-coupon/{id}','CouponsController@editCoupon');
Route::get('/admin/view-coupons','CouponsController@viewCoupons');
Route::get('/admin/delete-coupon/{id}','CouponsController@deleteCoupon');