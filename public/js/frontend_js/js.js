var MEWService=function() {
MEWService.initializeBase(this);
this._timeout = 0;
this._userContext = null;
this._succeeded = null;
this._failed = null;
}
MEWService.prototype={
_get_path:function() {
 var p = this.get_path();
 if (p) return p;
 else return MEWService._staticInstance.get_path();},
GetLastUpdate:function(EncContentDataId,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'GetLastUpdate',false,{EncContentDataId:EncContentDataId},succeededCallback,failedCallback,userContext); },
GetIntLastUpdate:function(succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'GetIntLastUpdate',false,{},succeededCallback,failedCallback,userContext); },
GetExchageRateGrid:function(isCurrency,SourceControl,From,ItemCount,LocalBoard,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'GetExchageRateGrid',false,{isCurrency:isCurrency,SourceControl:SourceControl,From:From,ItemCount:ItemCount,LocalBoard:LocalBoard},succeededCallback,failedCallback,userContext); },
CalculateExchangeRate:function(Amount,source,destination,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'CalculateExchangeRate',false,{Amount:Amount,source:source,destination:destination},succeededCallback,failedCallback,userContext); },
GetCurrenciesList:function(succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'GetCurrenciesList',false,{},succeededCallback,failedCallback,userContext); }}
MEWService.registerClass('MEWService',Sys.Net.WebServiceProxy);
MEWService._staticInstance = new MEWService();
MEWService.set_path = function(value) { MEWService._staticInstance.set_path(value); }
MEWService.get_path = function() { return MEWService._staticInstance.get_path(); }
MEWService.set_timeout = function(value) { MEWService._staticInstance.set_timeout(value); }
MEWService.get_timeout = function() { return MEWService._staticInstance.get_timeout(); }
MEWService.set_defaultUserContext = function(value) { MEWService._staticInstance.set_defaultUserContext(value); }
MEWService.get_defaultUserContext = function() { return MEWService._staticInstance.get_defaultUserContext(); }
MEWService.set_defaultSucceededCallback = function(value) { MEWService._staticInstance.set_defaultSucceededCallback(value); }
MEWService.get_defaultSucceededCallback = function() { return MEWService._staticInstance.get_defaultSucceededCallback(); }
MEWService.set_defaultFailedCallback = function(value) { MEWService._staticInstance.set_defaultFailedCallback(value); }
MEWService.get_defaultFailedCallback = function() { return MEWService._staticInstance.get_defaultFailedCallback(); }
MEWService.set_enableJsonp = function(value) { MEWService._staticInstance.set_enableJsonp(value); }
MEWService.get_enableJsonp = function() { return MEWService._staticInstance.get_enableJsonp(); }
MEWService.set_jsonpCallbackParameter = function(value) { MEWService._staticInstance.set_jsonpCallbackParameter(value); }
MEWService.get_jsonpCallbackParameter = function() { return MEWService._staticInstance.get_jsonpCallbackParameter(); }
MEWService.set_path("/SubSystems/Exchange/MEWService.asmx");
MEWService.GetLastUpdate= function(EncContentDataId,onSuccess,onFailed,userContext) {MEWService._staticInstance.GetLastUpdate(EncContentDataId,onSuccess,onFailed,userContext); }
MEWService.GetIntLastUpdate= function(onSuccess,onFailed,userContext) {MEWService._staticInstance.GetIntLastUpdate(onSuccess,onFailed,userContext); }
MEWService.GetExchageRateGrid= function(isCurrency,SourceControl,From,ItemCount,LocalBoard,onSuccess,onFailed,userContext) {MEWService._staticInstance.GetExchageRateGrid(isCurrency,SourceControl,From,ItemCount,LocalBoard,onSuccess,onFailed,userContext); }
MEWService.CalculateExchangeRate= function(Amount,source,destination,onSuccess,onFailed,userContext) {MEWService._staticInstance.CalculateExchangeRate(Amount,source,destination,onSuccess,onFailed,userContext); }
MEWService.GetCurrenciesList= function(onSuccess,onFailed,userContext) {MEWService._staticInstance.GetCurrenciesList(onSuccess,onFailed,userContext); }
var gtc = Sys.Net.WebServiceProxy._generateTypedConstructor;
if (typeof(ChartDataGrid) === 'undefined') {
var ChartDataGrid=gtc("ChartDataGrid");
ChartDataGrid.registerClass('ChartDataGrid');
}
