@extends('layouts.adminLayout.admin_design')
@section('content')

    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"><a href="index.html" title="برگشت به صفحه اصلی" class="tip-bottom"><i
                            class="icon-home"></i> پیشخوان</a> <a href="#">اسلایدر ها</a> <a href="#" class="current">ویرایش
                    اسلایدر</a></div>
            <h1>ویرایش اسلایدر</h1>
            @if(Session::has('flash_message_error'))
                <div class="alert alert-error alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('flash_message_error') !!}</strong>
                </div>
            @endif
            @if(Session::has('flash_message_success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('flash_message_success') !!}</strong>
                </div>
            @endif
        </div>
        <div class="container-fluid">
            <hr>
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"><span class="icon"> <i class="icon-info-sign"></i> </span>
                            <h5>ویرایش اسلایدر</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <form enctype="multipart/form-data" class="form-horizontal" method="post"
                                  action="{{ url('admin/edit-banner/'.$bannerDetails->id) }}" name="edit_banner"
                                  id="edit_banner" novalidate="novalidate">{{ csrf_field() }}
                                <div class="control-group">
                                    <label class="control-label">تصویر اسلایدر</label>
                                    <div class="controls">
                                        <div class="uploader" id="uniform-undefined"><input name="image" id="image"
                                                                                            type="file" size="19"
                                                                                            style="opacity: 0;"><span
                                                    class="filename">فایلی انتخاب نشد</span><span class="action">انتخاب فایل</span>
                                        </div>
                                        @if(!empty($bannerDetails->image))
                                            <input type="hidden" name="current_image"
                                                   value="{{ $bannerDetails->image }}">
                                        @endif
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">عنوان</label>
                                    <div class="controls">
                                        <input type="text" name="title" id="title" value="{{ $bannerDetails->title }}">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">لینک</label>
                                    <div class="controls">
                                        <input type="text" name="link" id="link" value="{{ $bannerDetails->link }}">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">فعال</label>
                                    <div class="controls">
                                        <input type="checkbox" name="status" id="status" value="1"
                                               @if($bannerDetails->status=="1") checked @endif>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <input type="submit" value="ویرایش اسلایدر" class="btn btn-success">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection