@extends('layouts.adminLayout.admin_design')
@section('content')

    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">اسلایدر ها</a> <a href="#" class="current">Vنمایش اسلایدرها</a> </div>
            <h1>اسلایدر</h1>
            @if(Session::has('flash_message_error'))
                <div class="alert alert-error alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('flash_message_error') !!}</strong>
                </div>
            @endif
            @if(Session::has('flash_message_success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('flash_message_success') !!}</strong>
                </div>
            @endif
        </div>
        <div class="container-fluid">
            <hr>
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                            <h5>نمایش اسلایدر ها</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered data-table">
                                <thead>
                                <tr>
                                    <th>لینک</th>
                                    <th>نام اسلایدر</th>
                                    <th>تصویر</th>
                                    <th>ویرایش</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($banners as $banner)
                                    <tr class="gradeX">
                                        <td>{{ $banner->link }}</td>
                                        <td>{{ $banner->title }}</td>
                                        <td>
                                            @if(!empty($banner->image))
                                                <img src="{{ asset('/images/frontend_images/banners/'.$banner->image) }}" style="width:250px;">
                                            @endif
                                        </td>
                                        <td class="center">
                                            <a href="{{ url('/admin/edit-banner/'.$banner->id) }}" class="btn btn-primary btn-mini">ویرایش</a>
                                            <a id="delCat" href="{{ url('/admin/delete-banner/'.$banner->id) }}" class="btn btn-danger btn-mini">حذف</a>
                                        </td>
                                    </tr>
                                    <div id="myModal{{ $banner->id }}" class="modal hide">
                                        <div class="modal-header">
                                            <button data-dismiss="modal" class="close" type="button">×</button>
                                            <h3>{{ $banner->banner_name }} Full Details</h3>
                                        </div>
                                        <div class="modal-body">
                                            <p>شماره اسلایدر: {{ $banner->id }}</p>
                                            <p>دسته بندی: {{ $banner->category_id }}</p>
                                            <p>کد اسلایدر: {{ $banner->banner_code }}</p>
                                            <p>رنگ اسلایدر: {{ $banner->banner_color }}</p>
                                            <p>قیمت: {{ $banner->price }}</p>
                                            <p>توضیح اسلایدر: {{ $banner->description }}</p>
                                        </div>
                                    </div>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection