{{-- File OF LaraVel View --}}
@extends('admin.app')
@section('content')
    <ul class="breadcrumb">
        <li><a href="#">داشبورد</a></li>
        <li><a href="#">ویدئو</a></li>
        <li class="active">ویدئو جدید</li>
    </ul>
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">ویرایش صفحه </h3>
                    </div>
                    <form action="{{ url('admin/edit', [$page->id]) }}" method="POST" role="form">

                        <div class="panel-body">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <label for="title">عنوان</label>
                                    <input type="text" class="form-control" name="title" id="title"
                                           value="{{ $page->title }}" placeholder="">
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf-token">
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <label for="">دسته بندی</label>
                                    <select name="category" id="category" title="دسته بندی"
                                            class="form-control select2">
                                        <option selected
{{--                                                value="{{ $page->page_category->first()->category_id }}">{{ \App\Models\Category::find($page->page_category->first()->category_id)->category }}</option>--}}
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <textarea name="description" class="summernote"
                                              title="">{{ $page->content }}</textarea>
                                </div>
                                <div class="col-md-12">
                                    <div class="panel panel-default" style="border: none">
                                        <div class="panel-body">
                                            <label for="tag" style="line-height: 30px;">برچسب</label>
                                            <select class="form-control select22" multiple="multiple" name="tags[]"
                                                    id="tag">
                                                @foreach($page->page_tag as $tag)
                                                    <option selected value="{{ $tag->tag }}"
                                                            data-select2-id="{{ $tag->id }}">{{ $tag->tag }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr/>
                            <div class="panel-heading">
                                <h3 class="panel-title"> امکانات سئو</h3>
                            </div>
                            <div class="panel-body" id="seo">
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <label for="title">عنوان</label>
                                    <input type="text" class="form-control" name="meta_title" id="title"
                                           value="{{ $page->page_seo->first()->title }}" placeholder="">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <label for="meta-tag">برچسب</label>
                                    <select class="form-control select22" multiple="multiple" name="meta_tags[]"
                                            id="meta-tag">
                                        @foreach(explode(', ', $page->page_seo->first()->tags) as $item)
                                            <option selected value="{{ $item }}">{{ $item }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <label for="desc" style="line-height: 30px;">توضیحات</label>
                                    <textarea id="desc" name="meta_description" class="form-control "
                                              title="">{{ $page->page_seo->first()->description }}</textarea>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary col-md-12">ثبت</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
