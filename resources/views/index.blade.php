@extends('layouts.frontLayout.front_design')

@section('content')

    <div id="OuterInlineContentsControl">
        <div id="InlineContentsControl" class="clearAlign  ">
            <div id="FirstSideZoneBackgroundTD"
                 class="leftalign col4 FirstSideZoneBackgroundTD DisplayBlock initialTextAlign">

            </div>
            <div id="TemplateHeader2ItemParent" class="leftalign col2 DisplayBlock initialTextAlign">
                <div id="MiddleContentsControl" class="clearAlign">
                    <div class="TemplateHeader2BackgroundTD" id="TH2ContentsControl">
                        <div class="w3-content w3-display-container"
                             style="max-width:1665px; max-height:562px;">
                            <img class="mySlides" src="{{ asset('images/frontend_images/home/slid1.png')}}" style="width:100%">
                            <img class="mySlides" src="{{ asset('images/frontend_images/home/slid2.png')}}" style="width:100%">
                            <div class="w3-center w3-container w3-section w3-large w3-text-white w3-display-bottommiddle"
                                 style="width:100%">
                                <div class="w3-left w3-hover-text-khaki" onclick="plusDivs(-1)">&#10094;
                                </div>
                                <div class="w3-right w3-hover-text-khaki" onclick="plusDivs(1)">&#10095;
                                </div>
                                <span class="w3-badge demo w3-border w3-transparent w3-hover-white"
                                      onclick="currentDiv(1)"></span>
                                <span class="w3-badge demo w3-border w3-transparent w3-hover-white"
                                      onclick="currentDiv(2)"></span>
                                <span class="w3-badge demo w3-border w3-transparent w3-hover-white"
                                      onclick="currentDiv(3)"></span>
                            </div>
                        </div>
                        <script>
                            var slideIndex = 1;
                            showDivs(slideIndex);

                            function plusDivs(n) {
                                showDivs(slideIndex += n);
                            }

                            function currentDiv(n) {
                                showDivs(slideIndex = n);
                            }

                            function showDivs(n) {
                                var i;
                                var x = document.getElementsByClassName("mySlides");
                                var dots = document.getElementsByClassName("demo");
                                if (n > x.length) {
                                    slideIndex = 1
                                }
                                if (n < 1) {
                                    slideIndex = x.length
                                }
                                for (i = 0; i < x.length; i++) {
                                    x[i].style.display = "none";
                                }
                                for (i = 0; i < dots.length; i++) {
                                    dots[i].className = dots[i].className.replace(" w3-white", "");
                                }
                                x[slideIndex - 1].style.display = "block";
                                dots[slideIndex - 1].className += " w3-white";
                            }
                        </script>
                        <div id="topholdnews" class="SBContainerPanels"
                             style="border-color:#f0f0f0;border-width:1px;border-style:Solid;-webkit-box-shadow:0px 0px 0px #777;-webkit-border-radius:0px;width:99%;-moz-border-radius:0px;box-shadow:0px 0px 0px #777;margin-left:10px;background-color:#fff;margin-bottom:-5px;border-radius:0px;margin-right:10px;max-width:1255px;margin-top:10px;height:288px;">

                            <div id="ctl28_iHeaderPanel" class="SBHeaderPanels"
                                 style="text-align:right;padding-top:0px;padding-left:0px;padding-right:0px;border-radius:-1px -1px 0px 0px;">

                                <span id="ctl28_iHeaderTextToView" class="aspNetDisabled"><h4>اهم اخبار</h4></span>

                            </div>
                            <div id="ctl28_iContentPanel" class="SBContentPanels"
                                 style="padding-top:0px;padding-left:0px;padding-right:0px;padding-bottom:0px;">

                                <div id="featured" class="ui-tabs ui-corner-all ui-widget ui-widget-content"
                                     style="position: initial;">
                                    <div class="featuredULHolder">
                                        <ul class="ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header"
                                            @foreach($products->my_products() as $i => $product)
                                            role="tablist">
                                            <li class="ui-tabs-nav-item  NewsImgs ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active"
                                                id="nav-fragment-1 " role="tab" tabindex="0"
                                                aria-controls="fragment-{{$i+1}}" aria-labelledby="ui-id-{{$i+1}}"
                                                aria-selected="true" aria-expanded="true"><a
                                                        href="#fragment-{{$i+1}}" title="یکشنبه 14 مهر 1398 03:18 ب.ظ"
                                                        role="presentation" tabindex="-1" class="ui-tabs-anchor"
                                                        id="ui-id-{{$i+1}}"><img style="width:40px"
                                                                          class="SliderNewsThumbnailImgs"
                                                                          src="{{ asset('/images/backend_images/product/large/'.$product->image) }}"><h6>{{$product->product_name}}ا</h6></a></li>
                                            @if($i == 2) @break @endif @endforeach
                                        </ul>

                                    </div>

                                    <div class="featuredContentsHolder">
                                        @foreach($products->my_products() as $i => $product)
                                        <div class="nopadding ui-tabs-panel rotatorcontents ui-corner-bottom ui-widget-content" id="fragment-{{$i+1}}" style="display: block; opacity: 0.916171;" aria-labelledby="ui-id-{{$i+1}}" role="tabpanel" aria-hidden="false">
                                            <div class="info">
                                                <img style="width:245px;height:183px;" class="NewsImgs" src="{{ asset('/images/backend_images/product/large/'.$product->image) }}">
                                                <a href="{{ url('/product/' . $product->id) }}"><h6>{{$product->product_name}}</h6></a>
                                            </div>
                                            <div id="RotatorSliderReadMore">
                                                <a href="{{ url('/product/' . $product->id) }}">بیشتر</a>
                                            </div>
                                        </div>
                                        @if($i == 2) @break @endif @endforeach
                                    </div>
                                </div>


                            </div>
                            <div id="ctl28_iFooterPanel" class="SBFooterPanels"
                                 style="text-align:left;border-radius:0px 0px -1px -1px;">

                                <span id="ctl28_iFooterTextToView"></span>

                            </div>

                        </div>


                    </div>
                    <div id="PageContainer">

                        <div id="HeaderContainer" class="headerBackgroundTD ">

                            <div id="IMHC1" class="leftalign col4 DisplayBlock">

                                <div id="akharinakhbarmob" class="SBContainerPanels"
                                     style="border-color:#f0f0f0;border-width:1px;border-style:Solid;-webkit-box-shadow:0px 0px 0px #777;-webkit-border-radius:0px;width:100%;-moz-border-radius:0px;box-shadow:0px 0px 0px #777;margin-left:0px;background-color:white;margin-bottom:7px;border-radius:0px;margin-right:0px;margin-top:7px;">

                                    <div id="ctl05_iHeaderPanel" class="SBHeaderPanels"
                                         style="text-align:right;padding-top:0px;padding-left:0px;padding-right:0px;border-radius:-1px -1px 0px 0px;">

                                        <span id="ctl05_iHeaderTextToView" class="aspNetDisabled"><h2>آخرین اخبار</h2></span>

                                    </div>
                                    <div id="ctl05_iContentPanel" class="SBContentPanels"
                                         style="padding-top:20px;padding-left:15px;padding-right:15px;padding-bottom:15px;">

                                        <div class="NewsLayout12Row" style="width:100%">
                                            <div class="Layout12NewsImgsDiv"><a
                                                        class="Layout12NewsLink tiptip"
                                                        href="#"><img
                                                            alt="لورم ایپسوم یا طرح‌نما"
                                                            class="Layout12NewsImgs"
                                                            src="{{ asset('images/frontend_images/home/ThumbnailImage_008.jpg')}}" '=""></a></div>
                                            <div class="NewsLayout12RowContents"><a
                                                        href="#"
                                                        class="tiptip"><h2>لورم ایپسوم یا طرح‌نما</h2></a>
                                                <div class="NewsLayout12RowReadMoreBtn"
                                                     style="display:none;"
                                                     onclick='window.location.href="#"'>
                                                    ادامه مطلب
                                                </div>
                                            </div>
                                        </div>
                                        <div style="clear:both"></div>
                                        <div class="NewsLayout12Row" style="width:100%">
                                            <div class="Layout12NewsImgsDiv"><a
                                                        class="Layout12NewsLink tiptip"
                                                        href="#/newsdetail.html?i=ZGpEM2drcVJXbVU9"><img
                                                            alt="لورم ایپسوم یا طرح‌نما"
                                                            class="Layout12NewsImgs"
                                                            src="{{ asset('images/frontend_images/home/ThumbnailImage_005.jpg')}}" '=""></a></div>
                                            <div class="NewsLayout12RowContents"><a
                                                        href="#/newsdetail.html?i=ZGpEM2drcVJXbVU9"
                                                        class="tiptip"><h2>لورم ایپسوم یا طرح‌نما</h2></a>
                                                <div class="NewsLayout12RowReadMoreBtn"
                                                     style="display:none;"
                                                     onclick='window.location.href="/newsdetail.html?i=ZGpEM2drcVJXbVU9"'>
                                                    ادامه مطلب
                                                </div>
                                            </div>
                                        </div>
                                        <div style="clear:both"></div>
                                        <div class="NewsLayout12Row" style="width:100%">
                                            <div class="Layout12NewsImgsDiv"><a
                                                        class="Layout12NewsLink tiptip"
                                                        href="#/newsdetail.html?i=dkpuMWR5YjNDOUk9"><img
                                                            alt="لورم ایپسوم یا طرح‌نما"
                                                            class="Layout12NewsImgs"
                                                            src="{{ asset('images/frontend_images/home/ThumbnailImage_006.jpg')}}" '=""></a></div>
                                            <div class="NewsLayout12RowContents"><a
                                                        href="#/newsdetail.html?i=dkpuMWR5YjNDOUk9"
                                                        class="tiptip"><h2>لورم ایپسوم یا طرح‌نما</h2></a>
                                                <div class="NewsLayout12RowReadMoreBtn"
                                                     style="display:none;"
                                                     onclick='window.location.href="/newsdetail.html?i=dkpuMWR5YjNDOUk9"'>
                                                    ادامه مطلب
                                                </div>
                                            </div>
                                        </div>
                                        <div style="clear:both"></div>
                                        <div class="NewsLayout12Row" style="width:100%">
                                            <div class="Layout12NewsImgsDiv"><a
                                                        class="Layout12NewsLink tiptip"
                                                        href="#/newsdetail.html?i=WEhlMVY0NVFoMUU9"><img
                                                            alt="لورم ایپسوم یا طرح‌نما"
                                                            class="Layout12NewsImgs"
                                                            src="{{ asset('images/frontend_images/home/ThumbnailImage.jpg')}}" '=""></a></div>
                                            <div class="NewsLayout12RowContents"><a
                                                        href="#/newsdetail.html?i=WEhlMVY0NVFoMUU9"
                                                        class="tiptip"><h2>لورم ایپسوم یا طرح‌نما</h2></a>
                                                <div class="NewsLayout12RowReadMoreBtn"
                                                     style="display:none;"
                                                     onclick='window.location.href="/newsdetail.html?i=WEhlMVY0NVFoMUU9"'>
                                                    ادامه مطلب
                                                </div>
                                            </div>
                                        </div>
                                        <div style="clear:both"></div>


                                    </div>
                                    <div id="ctl05_iFooterPanel" class="SBFooterPanels"
                                         style="text-align:left;border-radius:0px 0px -1px -1px;">

                                        <span id="ctl05_iFooterTextToView"></span>

                                    </div>

                                </div>


                            </div>


                            <div id="IMHC4" class="leftalign col4 DisplayBlock">


                            </div>
                            <div class="clearAlign"></div>

                        </div>

                        <div id="MenuContainer" class="MenuBackgroundTD">

                            <div id="IMMC1" class="leftalign col4 DisplayBlock">

                                <div id="ctl08_iViewMode" class="SBContainerPanels"
                                     style="border-color:#f0f0f0;border-width:1px;border-style:Solid;-webkit-box-shadow:0px 0px 0px #777;-webkit-border-radius:0px;width:100%;-moz-border-radius:0px;box-shadow:0px 0px 0px #777;margin-left:0px;background-color:#ffffff;margin-bottom:7px;border-radius:0px;margin-right:0px;max-width:303px;margin-top:7px;height:557px;">

                                    <div id="ctl08_iHeaderPanel" class="SBHeaderPanels"
                                         style="text-align:center;padding-top:0px;padding-left:0px;padding-right:0px;border-radius:-1px -1px 0px 0px;">

                                        <span id="ctl08_iHeaderTextToView" class="aspNetDisabled"><h4>لیست اخبار</h4></span>

                                    </div>
                                    <div id="ctl08_iContentPanel" class="SBContentPanels"
                                         style="padding-top:0px;padding-left:7px;padding-right:7px;padding-bottom:7px;">

                                        <div class="NewsLayout7">

                                            <div class="NewsLayout7MainNewsDiv"><a
                                                        href="#/newsdetail.html?i=RkM0QmtuRVZHLzA9"
                                                        class="tiptip"><img
                                                            alt="لورم ایپسوم یا طرح‌نما"
                                                            class="Layout7NewsImgs"
                                                            src="{{ asset('images/frontend_images/home/ThumbnailImage_002.jpg')}}"
                                                            style="margin-left:15px;width:260px;height:165px;"></a><a
                                                        href="#/newsdetail.html?i=RkM0QmtuRVZHLzA9"
                                                        class="tiptip"><h2>لورم ایپسوم یا طرح‌نما</h2></a>
                                                <p></p></div>
                                            <div class="NewsLayout7SubNewsDiv">
                                                <li><a href="#/newsdetail.html?i=dkpuMWR5YjNDOUk9"
                                                       class="tiptip">لورم ایپسوم یا طرح‌نما...</a></li>
                                                <li><a href="#/newsdetail.html?i=N25uaFdrZXI4NWM9"
                                                       class="tiptip">لورم ایپسوم یا طرح‌نما...</a></li>
                                            </div>
                                        </div>
                                        <div id="NewsArchivedCsvOWYzZHBOcDA9" class="NewsArchiveLink"><a
                                                    href="#/ForumNews.html">آرشیو</a></div>


                                    </div>
                                    <div id="ctl08_iFooterPanel" class="SBFooterPanels"
                                         style="text-align:left;border-radius:0px 0px -1px -1px;">

                                        <span id="ctl08_iFooterTextToView"></span>

                                    </div>

                                </div>

                                <div id="mystyle2" class="SBContainerPanels"
                                     style="border-color:#f0f0f0;border-width:1px;border-style:Solid;-webkit-box-shadow:0px 0px 0px #777;-webkit-border-radius:0px;width:100%;-moz-border-radius:0px;box-shadow:0px 0px 0px #777;margin-left:0px;background-color:#ffffff;margin-bottom:7px;border-radius:0px;margin-right:0px;max-width:303px;margin-top:7px;height:557px;">

                                    <div id="ctl09_iHeaderPanel" class="SBHeaderPanels"
                                         style="text-align:center;padding-top:0px;padding-left:0px;padding-right:0px;border-radius:-1px -1px 0px 0px;">

                                                    <span id="ctl09_iHeaderTextToView"
                                                          class="aspNetDisabled"><h4>اقتصاد</h4></span>

                                    </div>
                                    <div id="ctl09_iContentPanel" class="SBContentPanels"
                                         style="padding-top:0px;padding-left:7px;padding-right:7px;padding-bottom:7px;">

                                        <div class="NewsLayout7">
                                            @foreach($products->my_products_by_cat(1) as $i => $product)
                                            <div class="NewsLayout7MainNewsDiv"><a
                                                        href="{{ url('/product/' . $product->id) }}"
                                                        class="tiptip"><img
                                                            alt="{{$product->product_name}}ا"
                                                            class="Layout7NewsImgs"
                                                            src="{{ asset('/images/backend_images/product/large/'.$product->image) }}"
                                                            style="margin-left:15px;width:260px;height:165px;"></a><a
                                                        href="{{ url('/product/' . $product->id) }}"
                                                        class="tiptip"><h2>{{$product->product_name}}</h2></a>
                                                <p></p></div>
                                            <div class="NewsLayout7SubNewsDiv">
                                                <li><a href="{{ url('/product/' . $product->id) }}"
                                                       class="tiptip">«لورم ایپسوم یا طرح‌نم
                                                        ا</a></li>
                                                <li><a href="#/newsdetail.html?i=eFIxb0Fib25icFE9"
                                                       class="tiptip">لورم ایپسوم یا طرح‌نما...</a></li>
                                            </div>
                                            @if($i == 0) @break @endif @endforeach
                                        </div>

                                        <div id="NewsArchiveS1gwSzhSZ3UzLzg9" class="NewsArchiveLink"><a
                                                    href="#/Economic.html">آرشیو</a></div>


                                    </div>
                                    <div id="ctl09_iFooterPanel" class="SBFooterPanels"
                                         style="text-align:left;border-radius:0px 0px -1px -1px;">

                                        <span id="ctl09_iFooterTextToView"></span>

                                    </div>

                                </div>


                            </div>
                            <div id="IMMC2" class="leftalign col4 DisplayBlock">

                                <div id="ctl10_iViewMode" class="SBContainerPanels"
                                     style="border-color:#f0f0f0;border-width:1px;border-style:Solid;-webkit-box-shadow:0px 0px 0px #777;-webkit-border-radius:0px;width:100%;-moz-border-radius:0px;box-shadow:0px 0px 0px #777;margin-left:0px;background-color:#ffffff;margin-bottom:7px;border-radius:0px;margin-right:0px;max-width:303px;margin-top:7px;height:557px;">

                                    <div id="ctl10_iHeaderPanel" class="SBHeaderPanels"
                                         style="text-align:center;padding-top:0px;padding-left:0px;padding-right:0px;border-radius:-1px -1px 0px 0px;">

                                        <span id="ctl10_iHeaderTextToView" class="aspNetDisabled"><h4>اخبار علمی</h4></span>

                                    </div>
                                    <div id="ctl10_iContentPanel" class="SBContentPanels"
                                         style="padding-top:0px;padding-left:7px;padding-right:7px;padding-bottom:7px;">

                                        <div class="NewsLayout7">
                                            <div class="NewsLayout7MainNewsDiv"><a
                                                        href="#/newsdetail.html?i=a1VMMDdEQm1oL3c9"
                                                        class="tiptip"><img
                                                            alt="لورم ایپسوم یا طرح‌نما"
                                                            class="Layout7NewsImgs"
                                                            src="{{ asset('images/frontend_images/home/khbr2.png')}}"
                                                            style="margin-left:15px;width:260px;height:165px;"></a><a
                                                        href="#/newsdetail.html?i=a1VMMDdEQm1oL3c9"
                                                        class="tiptip"><h2>یپسوم یا طرح‌نمامی‌کند؟</h2></a>
                                                <p></p></div>
                                            <div class="NewsLayout7SubNewsDiv">
                                                <li><a href="#/newsdetail.html?i=dStPMTQyektUeG89"
                                                       class="tiptip">لورم ایپسوم یا طرح‌نما
                                                        ...</a></li>
                                                <li><a href="#/newsdetail.html?i=bnF1U0I3NzZJdzg9"
                                                       class="tiptip">لورم ایپسوم یا طرح‌نما</a></li>
                                            </div>
                                        </div>
                                        <div id="NewsArchiveUmNqVVA0SnJSWUU9" class="NewsArchiveLink"><a
                                                    href="#/oilandhealth.html">آرشیو</a></div>


                                    </div>
                                    <div id="ctl10_iFooterPanel" class="SBFooterPanels"
                                         style="text-align:left;border-radius:0px 0px -1px -1px;">

                                        <span id="ctl10_iFooterTextToView"></span>

                                    </div>

                                </div>
                                <div id="ctl10_iViewMode" class="SBContainerPanels" style="border-color:#f0f0f0;border-width:1px;border-style:Solid;-webkit-box-shadow:0px 0px 0px #777;-webkit-border-radius:0px;width:100%;-moz-border-radius:0px;box-shadow:0px 0px 0px #777;margin-left:0px;background-color:#ffffff;margin-bottom:7px;border-radius:0px;margin-right:0px;max-width:303px;margin-top:7px;height:557px;">

                                    <div id="ctl10_iHeaderPanel" class="SBHeaderPanels" style="text-align:center;padding-top:0px;padding-left:0px;padding-right:0px;border-radius:-1px -1px 0px 0px;">

                                        <span id="ctl10_iHeaderTextToView" class="aspNetDisabled"><h4>اقتصاد داخلی</h4></span>

                                    </div>
                                    <div id="ctl10_iContentPanel" class="SBContentPanels" style="padding-top:0px;padding-left:7px;padding-right:7px;padding-bottom:7px;">

                                        <div class="NewsLayout7">
                                            <div class="NewsLayout7MainNewsDiv"><a href="#/newsdetail.html?i=a1VMMDdEQm1oL3c9" class="tiptip"><img alt="لورم ایپسوم یا طرح‌نما" class="Layout7NewsImgs" src="morteza/khbr3.png" style="margin-left:15px;width:260px;height:165px;"></a><a href="#/newsdetail.html?i=a1VMMDdEQm1oL3c9" class="tiptip"><h2>لورم ایپسوم یا طرح‌نما
                                                        ...</h2></a>
                                                <p></p></div>
                                            <div class="NewsLayout7SubNewsDiv">
                                                <li><a href="#/newsdetail.html?i=dStPMTQyektUeG89" class="tiptip">لورم ایپسوم یا طرح‌نما
                                                        ...</a></li>
                                                <li><a href="#/newsdetail.html?i=bnF1U0I3NzZJdzg9" class="tiptip"> لورم ایپسوم یا طرح‌نما
                                                        ...</a></li>
                                            </div>
                                        </div>
                                        <div id="NewsArchiveUmNqVVA0SnJSWUU9" class="NewsArchiveLink"><a href="#/oilandhealth.html">آرشیو</a></div>


                                    </div>
                                    <div id="ctl10_iFooterPanel" class="SBFooterPanels" style="text-align:left;border-radius:0px 0px -1px -1px;">

                                        <span id="ctl10_iFooterTextToView"></span>

                                    </div>

                                </div>
                            </div>
                            <div id="IMMC3" class="leftalign col4 DisplayBlock">

                                <div id="ctl13_iViewMode" class="SBContainerPanels"
                                     style="border-color:#f0f0f0;border-width:1px;border-style:Solid;-webkit-box-shadow:0px 0px 0px #777;-webkit-border-radius:0px;width:100%;-moz-border-radius:0px;box-shadow:0px 0px 0px #777;margin-left:0px;background-color:#ffffff;margin-bottom:7px;border-radius:0px;margin-right:0px;max-width:303px;margin-top:7px;height:557px;">

                                    <div id="ctl13_iHeaderPanel" class="SBHeaderPanels"
                                         style="text-align:center;padding-top:0px;padding-left:0px;padding-right:0px;border-radius:-1px -1px 0px 0px;">

                                        <span id="ctl13_iHeaderTextToView" class="aspNetDisabled"><h4>بین الملل</h4></span>

                                    </div>
                                    <div id="ctl13_iContentPanel" class="SBContentPanels"
                                         style="padding-top:0px;padding-left:7px;padding-right:7px;padding-bottom:7px;">

                                        <div class="NewsLayout7">
                                            <div class="NewsLayout7MainNewsDiv"><a
                                                        href="#/newsdetail.html?i=RFpkSVN3aU11aTA9"
                                                        class="tiptip"><img
                                                            alt="دانه‌های سویای روی دست کشاورزان آمریکایی مانده است"
                                                            class="Layout7NewsImgs"
                                                            src="{{('images/frontend_images/home/khbr1.png')}}"
                                                            style="margin-left:15px;width:260px;height:165px;"></a><a
                                                        href="#/newsdetail.html?i=RFpkSVN3aU11aTA9"
                                                        class="tiptip"><h2>لورم ایپسوم یا طرح‌نما</h2></a>
                                                <p></p></div>
                                            <div class="NewsLayout7SubNewsDiv">
                                                <li><a href="#/newsdetail.html?i=ZHJUZE9oNjhzcHM9"
                                                       class="tiptip">لورم ایپسوم یا طرح‌نما...</a></li>
                                                <li><a href="#/newsdetail.html?i=bGM0RUhTT08vdEU9"
                                                       class="tiptip">لورم ایپسوم یا طرح‌نما..</a></li>
                                            </div>
                                        </div>
                                        <div id="NewsArchiveNFdIVGhyV2Zxd2M9" class="NewsArchiveLink"><a
                                                    href="#/TheInternational.html">آرشیو</a></div>


                                    </div>
                                    <div id="ctl13_iFooterPanel" class="SBFooterPanels"
                                         style="text-align:left;border-radius:0px 0px -1px -1px;">

                                        <span id="ctl13_iFooterTextToView"></span>

                                    </div>

                                </div>
                                <div id="ctl10_iViewMode" class="SBContainerPanels" style="border-color:#f0f0f0;border-width:1px;border-style:Solid;-webkit-box-shadow:0px 0px 0px #777;-webkit-border-radius:0px;width:100%;-moz-border-radius:0px;box-shadow:0px 0px 0px #777;margin-left:0px;background-color:#ffffff;margin-bottom:7px;border-radius:0px;margin-right:0px;max-width:303px;margin-top:7px;height:557px;">

                                    <div id="ctl10_iHeaderPanel" class="SBHeaderPanels" style="text-align:center;padding-top:0px;padding-left:0px;padding-right:0px;border-radius:-1px -1px 0px 0px;">

                                        <span id="ctl10_iHeaderTextToView" class="aspNetDisabled"><h4>اخبار واردات دام و طیور</h4></span>

                                    </div>
                                    <div id="ctl10_iContentPanel" class="SBContentPanels" style="padding-top:0px;padding-left:7px;padding-right:7px;padding-bottom:7px;">

                                        <div class="NewsLayout7">
                                            <div class="NewsLayout7MainNewsDiv"><a href="#/newsdetail.html?i=a1VMMDdEQm1oL3c9" class="tiptip"><img alt="لورم ایپسوم یا طرح‌نما" class="Layout7NewsImgs" src="morteza/khbr4.png" style="margin-left:15px;width:260px;height:165px;"></a><a href="#/newsdetail.html?i=a1VMMDdEQm1oL3c9" class="tiptip"><h2>لورم ایپسوم یا طرح‌نما
                                                        ...</h2></a>
                                                <p></p></div>
                                            <div class="NewsLayout7SubNewsDiv">
                                                <li><a href="#/newsdetail.html?i=dStPMTQyektUeG89" class="tiptip">لورم ایپسوم یا طرح‌نما
                                                        ...</a></li>
                                                <li><a href="#/newsdetail.html?i=bnF1U0I3NzZJdzg9" class="tiptip"> لورم ایپسوم یا طرح‌نما
                                                        ...</a></li>
                                            </div>
                                        </div>
                                        <div id="NewsArchiveUmNqVVA0SnJSWUU9" class="NewsArchiveLink"><a href="#/oilandhealth.html">آرشیو</a></div>


                                    </div>
                                    <div id="ctl10_iFooterPanel" class="SBFooterPanels" style="text-align:left;border-radius:0px 0px -1px -1px;">

                                        <span id="ctl10_iFooterTextToView"></span>

                                    </div>

                                </div>
                            </div>
                            <div id="IMMC4" class="leftalign col4 DisplayBlock">

                                <div class="CMSPlusContentPageTabs ui-tabs ui-corner-all ui-widget ui-widget-content"
                                     style="position: initial;">
                                    <ul role="tablist"
                                        class="ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header">
                                        <li role="tab" tabindex="0"
                                            class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active"
                                            aria-controls="CMSPlusContentTabMWViZWVORTI4d289"
                                            aria-labelledby="ui-id-4" aria-selected="true"
                                            aria-expanded="true"><a
                                                    href="#CMSPlusContentTabMWViZWVORTI4d289"
                                                    role="presentation" tabindex="-1" class="ui-tabs-anchor"
                                                    id="ui-id-4"></a></li>
                                    </ul>
                                    <div id="CMSPlusContentTabMWViZWVORTI4d289" aria-labelledby="ui-id-4"
                                         role="tabpanel"
                                         class="ui-tabs-panel ui-corner-bottom ui-widget-content"
                                         aria-hidden="false">
                                        <div id="ctl16_iViewMode"
                                             style="border-color:#f0f0f0;border-width:1px;border-style:Solid;-webkit-box-shadow:0px 0px 0px #777;-webkit-border-radius:0px;-moz-border-radius:0px;box-shadow:0px 0px 0px #777;margin-left:0px;background-color:#ffffff;margin-bottom:7px;border-radius:0px;margin-right:0px;margin-top:7px;">

                                            <div id="ctl16_iHeaderPanel"
                                                 style="border-radius:-1px -1px 0px 0px;">

                                                            <span id="ctl16_iHeaderTextToView"
                                                                  class="aspNetDisabled"></span>

                                            </div>
                                            <div id="ctl16_iContentPanel"
                                                 style="padding-top:7px;padding-left:7px;padding-right:7px;padding-bottom:7px;">

                                                <p style="text-align: center !important;"><img alt=""
                                                                                               src="{{ asset('images/frontend_images/home/1-76-1.jpg')}}"
                                                                                               width="72"
                                                                                               height="99">
                                                </p>


                                            </div>
                                            <div id="ctl16_iFooterPanel"
                                                 style="text-align:left;border-radius:0px 0px -1px -1px;">

                                                <span id="ctl16_iFooterTextToView"></span>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <div id="mystyle" class="SBContainerPanels"
                                     style="border-color:#f0f0f0;border-width:1px;border-style:Solid;-webkit-box-shadow:0px 0px 0px #777;-webkit-border-radius:0px;-moz-border-radius:0px;box-shadow:0px 0px 0px #777;margin-left:0px;background-color:white;margin-bottom:7px;border-radius:0px;margin-right:0px;margin-top:7px;">

                                    <div id="ctl17_iHeaderPanel" class="SBHeaderPanels"
                                         style="text-align:center;padding-top:0px;padding-left:0px;padding-right:0px;border-radius:-1px -1px 0px 0px;">

                                        <span id="ctl17_iHeaderTextToView" class="aspNetDisabled"><h4>سمینارها و همایش ها</h4></span>

                                    </div>
                                    <div id="ctl17_iContentPanel" class="SBContentPanels"
                                         style="padding-top:0px;padding-left:20px;padding-right:10px;padding-bottom:10px;">

                                        <p style="text-align: center !important;"><a href="#"><img alt=""
                                                                                                   src="{{ asset('images/frontend_images/home/courses.jpg')}}"
                                                                                                   style="max-width: 310px; width: 80%;"></a>
                                        </p>


                                    </div>
                                    <div id="ctl17_iFooterPanel" class="SBFooterPanels"
                                         style="text-align:left;border-radius:0px 0px -1px -1px;">

                                        <span id="ctl17_iFooterTextToView"></span>

                                    </div>

                                </div>


                                <div id="ctl19_iViewMode" class="SBContainerPanels"
                                     style="border-color:#f0f0f0;border-width:1px;border-style:Solid;-webkit-box-shadow:0px 0px 0px #777;-webkit-border-radius:0px;-moz-border-radius:0px;box-shadow:0px 0px 0px #777;margin-left:0px;background-color:white;margin-bottom:7px;border-radius:0px;margin-right:0px;margin-top:7px;">

                                    <div id="ctl19_iHeaderPanel" class="SBHeaderPanels"
                                         style="text-align:center;padding-top:0px;padding-left:0px;padding-right:0px;border-radius:-1px -1px 0px 0px;">

                                        <span id="ctl19_iHeaderTextToView" class="aspNetDisabled"><h4>قیمت مصوب</h4></span>

                                    </div>
                                    <div id="ctl19_iContentPanel" class="SBContentPanels"
                                         style="padding-top:10px;padding-left:10px;padding-right:10px;padding-bottom:10px;">

                                        <p style="text-align: center !important;"><a href="#"><img alt=""
                                                                                                   src="{{ asset('images/frontend_images/home/oil-price.png')}}"
                                                                                                   style="max-width: 220px; width:90%;"></a>
                                        </p>


                                    </div>
                                    <div id="ctl19_iFooterPanel" class="SBFooterPanels"
                                         style="text-align:left;border-radius:0px 0px -1px -1px;">

                                        <span id="ctl19_iFooterTextToView"></span>

                                    </div>

                                </div>

                                <div id="mystyle" class="SBContainerPanels"
                                     style="border-color:#f0f0f0;border-width:1px;border-style:Solid;-webkit-box-shadow:0px 0px 0px #777;-webkit-border-radius:0px;-moz-border-radius:0px;box-shadow:0px 0px 0px #777;margin-left:0px;background-color:white;margin-bottom:7px;border-radius:0px;margin-right:0px;margin-top:7px;">

                                    <div id="ctl20_iHeaderPanel" class="SBHeaderPanels"
                                         style="text-align:center;padding-top:0px;padding-left:0px;padding-right:0px;border-radius:-1px -1px 0px 0px;">

                                        <span id="ctl20_iHeaderTextToView" class="aspNetDisabled"><h4>لورم ایپسوم یا طرح‌نما</h4></span>

                                    </div>
                                    <div id="ctl20_iContentPanel" class="SBContentPanels"
                                         style="padding-top:0px;padding-left:20px;padding-right:10px;padding-bottom:10px;">

                                        <ol>
                                            <li>لورم ایپسوم یا طرح‌نما</li>
                                            <li>لورم ایپسوم یا طرح‌نما</li>
                                            <li>لورم ایپسوم یا طرح‌نما</li>
                                            <li>لورم ایپسوم یا طرح‌نما</li>
                                            <li>لورم ایپسوم یا طرح‌نما</li>
                                            <li>لورم ایپسوم یا طرح‌نما</li>
                                            <li>لورم ایپسوم یا طرح‌نما</li>
                                            <li>لورم ایپسوم یا طرح‌نما</li>
                                            <li>لورم ایپسوم یا طرح‌نما</li>
                                        </ol>

                                        <p style="    text-align: left !important;">&nbsp;</p>


                                    </div>
                                    <div id="ctl20_iFooterPanel" class="SBFooterPanels"
                                         style="text-align:left;border-radius:0px 0px -1px -1px;">

                                        <span id="ctl20_iFooterTextToView"></span>

                                    </div>

                                </div>

                                <div id="mystyle" class="SBContainerPanels"
                                     style="border-color:#f0f0f0;border-width:1px;border-style:Solid;-webkit-box-shadow:0px 0px 0px #777;-webkit-border-radius:0px;-moz-border-radius:0px;box-shadow:0px 0px 0px #777;margin-left:0px;background-color:white;margin-bottom:7px;border-radius:0px;margin-right:0px;margin-top:7px;">

                                    <div id="ctl21_iHeaderPanel" class="SBHeaderPanels"
                                         style="text-align:center;padding-top:0px;padding-left:0px;padding-right:0px;border-radius:-1px -1px 0px 0px;">

                                        <span id="ctl21_iHeaderTextToView" class="aspNetDisabled"><h4>آئین نامه ها و بخشنامه ها</h4></span>

                                    </div>
                                    <div id="ctl21_iContentPanel" class="SBContentPanels"
                                         style="padding-top:0px;padding-left:20px;padding-right:10px;padding-bottom:10px;">

                                        <ul>
                                            <li><a href="#/newsdetail.html?i=bHpYN2NjWlFhVk09"
                                                   class="tiptip">لورم ایپسوم یا طرح‌نما</a></li>
                                            <li><a href="#/newsdetail.html?i=TDBJeStjOTJKaFk9"
                                                   class="tiptip">لورم ایپسوم یا طرح‌نما</a></li>
                                        </ul>


                                    </div>
                                    <div id="ctl21_iFooterPanel" class="SBFooterPanels"
                                         style="text-align:left;border-radius:0px 0px -1px -1px;">

                                        <span id="ctl21_iFooterTextToView"></span>

                                    </div>

                                </div>


                            </div>
                            <div class="clearAlign"></div>

                        </div>
                    </div>
                    <div id="TF2ContentsControl" class="TemplateFooter2BackgroundTD">
                        <div id="mystyle" class="SBContainerPanels"
                             style="border-color:#f0f0f0;border-width:1px;border-style:Solid;-webkit-box-shadow:0px 0px 0px #777;-webkit-border-radius:0px;width:99%;-moz-border-radius:0px;box-shadow:0px 0px 0px #777;margin-left:0px;background-color:white;margin-bottom:7px;border-radius:0px;margin-right:0px;max-width:1235px;margin-top:7px;">

                            <div id="ctl30_iHeaderPanel" class="SBHeaderPanels"
                                 style="text-align:right;padding-top:0px;padding-left:0px;padding-right:0px;border-radius:-1px -1px 0px 0px;">

                                            <span id="ctl30_iHeaderTextToView"
                                                  class="aspNetDisabled"><h4>اعضای انجمن</h4></span>
                            </div>
                            <div id="ctl30_iContentPanel" class="SBContentPanels"
                                 style="padding-top:2px;padding-left:15px;padding-right:15px;padding-bottom:4px;">
                                <div style="width:90%;margin:0 auto;">
                                    <style type="text/css">#Slicker img {
                                            height: 75px;
                                        }
                                    </style>
                                    <div id="Slicker" class="slick-initialized slick-slider">
                                        <button type="button" data-role="none"
                                                class="slick-prev slick-arrow" aria-label="Previous"
                                                role="button" style="display: block;">Previous
                                        </button>
                                        <div aria-live="polite" class="slick-list draggable">
                                            <div class="slick-track"
                                                 style="opacity: 1; width: 10900px; transform: translate3d(7303px, 0px, 0px);"
                                                 role="listbox">
                                                <div class="slick-slide slick-cloned" data-slick-index="-10"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1">
                                                    <a href="#/?p=13364" target="_blank" tabindex="-1"><img
                                                                alt="شرکت روغن نباتی ناز اصفهان"
                                                                src="{{ asset('images/frontend_images/home/0-5307-150x150.jpg')}}"
                                                                title="شرکت روغن نباتی ناز اصفهان" width="75"
                                                                height="75"></a></div>
                                                <div class="slick-slide slick-cloned" data-slick-index="-9"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1">
                                                    <a href="#/?p=13039" target="_blank" tabindex="-1"><img
                                                                alt="" src="{{ asset('images/frontend_images/home/0-5267-150x150.jpg')}}" title=""
                                                                width="75" height="75"></a></div>
                                                <div class="slick-slide slick-cloned" data-slick-index="-8"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1">
                                                    <a href="#/?p=13314" target="_blank" tabindex="-1"><img
                                                                alt="شرکت گلبهار سپاهان"
                                                                src="{{ asset('images/frontend_images/home/0-5303-150x150.jpg')}}"
                                                                title="گلبهار سپاهان" width="75"
                                                                height="75"></a></div>
                                                <div class="slick-slide slick-cloned" data-slick-index="-7"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1">
                                                    <img alt="" src="{{ asset('images/frontend_images/home/logo-150x150.gif')}}"
                                                         title="شرکت زیتون طلایی" width="75" height="75">
                                                </div>
                                                <div class="slick-slide slick-cloned" data-slick-index="-6"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1">
                                                    <a href="#/?p=12642" target="_blank" tabindex="-1"><img
                                                                alt="شرکت گلبهار پارسیان"
                                                                src="{{ asset('images/frontend_images/home/LOGO-GOLBAHARPARSIAN-whit-150x150.jpg')}}"
                                                                title="شرکت گلبهار پارسیان" width="75"
                                                                height="75"></a></div>
                                                <div class="slick-slide slick-cloned" data-slick-index="-5"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1">
                                                    <a href="#/?p=11525" target="_blank" tabindex="-1"><img
                                                                alt="شرکت گلستان"
                                                                src="{{ asset('images/frontend_images/home/0-3461-150x118.jpg')}}"
                                                                title="شرکت گلستان" width="75" height="59"></a>
                                                </div>
                                                <div class="slick-slide slick-cloned" data-slick-index="-4"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1">
                                                    <a href="#/?p=11118" target="_blank" tabindex="-1"><img
                                                                alt="لابراتوار داروئی و غذائی بالک"
                                                                src="{{ asset('images/frontend_images/home/verina-logo.png')}}"
                                                                title="لابراتوار داروئی و غذائی بالک" width="75"
                                                                height="75"></a></div>
                                                <div class="slick-slide slick-cloned" data-slick-index="-3"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1">
                                                    <a href="#/?p=10386" target="_blank" tabindex="-1"><img
                                                                alt="مجتمع مخازن روغنی خلیج فارس"
                                                                src="{{ asset('images/frontend_images/home/persian-gulf-logo.png')}}"
                                                                title="مجتمع مخازن روغنی خلیج فارس" width="52"
                                                                height="75"></a></div>
                                                <div class="slick-slide slick-cloned" data-slick-index="-2"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1">
                                                    <img alt="آسیا فراز ایرانیان "
                                                         src="{{ asset('images/frontend_images/home/asia-logo.png')}}"
                                                         title="آسیا فراز ایرانیان" width="75" height="75">
                                                </div>
                                                <div class="slick-slide slick-cloned" data-slick-index="-1"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1">
                                                    <a href="#/?p=10328" target="_blank" tabindex="-1"><img
                                                                alt="آلتن دانه صحرا"
                                                                src="{{ asset('images/frontend_images/home/bayragh-logo.png')}}"
                                                                title="آلتن دانه صحرا" width="75"
                                                                height="75"></a></div>
                                                <div class="slick-slide" data-slick-index="0"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide00"><img
                                                            alt="حیات هرمزگان" src="{{ asset('images/frontend_images/home/hayat-logo.png')}}"
                                                            title="حیات هرمزگان" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="1"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide01"><img
                                                            alt="ماهیدشت کرمانشاه"
                                                            src="{{ asset('images/frontend_images/home/mahidasht-logo.png')}}"
                                                            title="ماهیدشت کرمانشاه" width="75" height="75">
                                                </div>
                                                <div class="slick-slide" data-slick-index="2"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide02"><a
                                                            href="#/?p=9758" target="_blank" tabindex="-1"><img
                                                                alt="گلناز کرمان" src="{{ asset('images/frontend_images/home/golnaz-logo.png')}}"
                                                                title="گلناز کرمان" width="75" height="42"></a>
                                                </div>
                                                <div class="slick-slide" data-slick-index="3"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide03"><img
                                                            alt="گل سفید البرز" src="{{ asset('images/frontend_images/home/golsefid-logo.png')}}"
                                                            title="گل سفید البرز" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="4"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide04"><a
                                                            href="#/?p=11577" target="_blank" tabindex="-1"><img
                                                                alt="روغن نباتی اردبیل"
                                                                src="{{ asset('images/frontend_images/home/ardebil-logo.png')}}"
                                                                title="روغن نباتی اردبیل" width="75"
                                                                height="75"></a></div>
                                                <div class="slick-slide" data-slick-index="5"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide05"><img
                                                            alt="صنعتی بهپاک" src="{{ asset('images/frontend_images/home/behpak-logo.png')}}"
                                                            title="صنعتی بهپاک" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="6"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide06"><img
                                                            alt=" ناب دانه سمنان" src="{{ asset('images/frontend_images/home/nabdane-logo.png')}}"
                                                            title="ناب دانه سمنان" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="7"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide07"><a
                                                            href="#/?p=10389" target="_blank" tabindex="-1"><img
                                                                alt="گیتا طلایی گلستان" src="{{ asset('images/frontend_images/home/giti-logo.png')}}"
                                                                title="گیتا طلایی گلستان" width="75"
                                                                height="43"></a></div>
                                                <div class="slick-slide" data-slick-index="8"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide08"><a
                                                            href="#/?p=10951" target="_blank" tabindex="-1"><img
                                                                alt="مارگارین" src="{{ asset('images/frontend_images/home/margarin-logo.png')}}"
                                                                title="مارگارین" width="65" height="75"></a></div>
                                                <div class="slick-slide" data-slick-index="9"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide09"><img
                                                            alt="طلای سفید گنبد" src="{{ asset('images/frontend_images/home/talsefid-logo.png')}}"
                                                            title="طلای سفید گنبد" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="10"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide010"><img
                                                            alt="فضل نیشابور" src="{{ asset('images/frontend_images/home/fazl-logo.jpg')}}"
                                                            title="فضل نیشابور" width="75" height="53"></div>
                                                <div class="slick-slide" data-slick-index="11"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide011"><a
                                                            href="#/?p=12965" target="_blank" tabindex="-1"><img
                                                                alt="شرکت فرآوری سبوس کندوج"
                                                                src="{{ asset('images/frontend_images/home/kandooj-2-150x150.jpg')}}"
                                                                title="فرآوری سبوس کندوج" width="75"
                                                                height="75"></a></div>
                                                <div class="slick-slide" data-slick-index="12"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide012"><a
                                                            href="#/?p=13364" target="_blank" tabindex="-1"><img
                                                                alt="شرکت روغن نباتی ناز اصفهان"
                                                                src="{{ asset('images/frontend_images/home/0-5307-150x150.jpg')}}"
                                                                title="شرکت روغن نباتی ناز اصفهان" width="75"
                                                                height="75"></a></div>
                                                <div class="slick-slide" data-slick-index="13"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide013"><a
                                                            href="#/?p=13039" target="_blank" tabindex="-1"><img
                                                                alt="" src="{{ asset('images/frontend_images/home/0-5267-150x150.jpg')}}" title=""
                                                                width="75" height="75"></a></div>
                                                <div class="slick-slide" data-slick-index="14"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide014"><a
                                                            href="#/?p=13314" target="_blank" tabindex="-1"><img
                                                                alt="شرکت گلبهار سپاهان"
                                                                src="{{ asset('images/frontend_images/home/0-5303-150x150.jpg')}}"
                                                                title="گلبهار سپاهان" width="75" height="75"></a>
                                                </div>
                                                <div class="slick-slide" data-slick-index="15"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide015"><img
                                                            alt="" src="{{ asset('images/frontend_images/home/logo-150x150.gif')}}"
                                                            title="شرکت زیتون طلایی" width="75" height="75">
                                                </div>
                                                <div class="slick-slide" data-slick-index="16"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide016"><a
                                                            href="#/?p=12642" target="_blank" tabindex="-1"><img
                                                                alt="شرکت گلبهار پارسیان"
                                                                src="{{ asset('images/frontend_images/home/LOGO-GOLBAHARPARSIAN-whit-150x150.jpg')}}"
                                                                title="شرکت گلبهار پارسیان" width="75" height="75"></a>
                                                </div>
                                                <div class="slick-slide" data-slick-index="17"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide017"><a
                                                            href="#/?p=11525" target="_blank" tabindex="-1"><img
                                                                alt="شرکت گلستان" src="{{ asset('images/frontend_images/home/0-3461-150x118.jpg')}}"
                                                                title="شرکت گلستان" width="75" height="59"></a>
                                                </div>
                                                <div class="slick-slide" data-slick-index="18"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide018"><a
                                                            href="#/?p=11118" target="_blank" tabindex="-1"><img
                                                                alt="لابراتوار داروئی و غذائی بالک"
                                                                src="{{ asset('images/frontend_images/home/verina-logo.png')}}"
                                                                title="لابراتوار داروئی و غذائی بالک" width="75"
                                                                height="75"></a></div>
                                                <div class="slick-slide" data-slick-index="19"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide019"><a
                                                            href="#/?p=10386" target="_blank" tabindex="-1"><img
                                                                alt="مجتمع مخازن روغنی خلیج فارس"
                                                                src="{{ asset('images/frontend_images/home/persian-gulf-logo.png')}}"
                                                                title="مجتمع مخازن روغنی خلیج فارس" width="52"
                                                                height="75"></a></div>
                                                <div class="slick-slide" data-slick-index="20"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide020"><img
                                                            alt="آسیا فراز ایرانیان "
                                                            src="{{ asset('images/frontend_images/home/asia-logo.png')}}"
                                                            title="آسیا فراز ایرانیان" width="75" height="75">
                                                </div>
                                                <div class="slick-slide" data-slick-index="21"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide021"><a
                                                            href="#/?p=10328" target="_blank" tabindex="-1"><img
                                                                alt="آلتن دانه صحرا" src="{{ asset('images/frontend_images/home/bayragh-logo.png')}}"
                                                                title="آلتن دانه صحرا" width="75" height="75"></a>
                                                </div>
                                                <div class="slick-slide" data-slick-index="22"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide022"><a
                                                            href="#/?p=10299" target="_blank" tabindex="-1"><img
                                                                alt="شیمی آلایش شمال" src="{{ asset('images/frontend_images/home/alayesh-logo.png')}}"
                                                                title="شیمی آلایش شمال" width="75" height="74"></a>
                                                </div>
                                                <div class="slick-slide" data-slick-index="23"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide023"><img
                                                            alt=" صنعتی بهشهر" src="{{ asset('images/frontend_images/home/behshahr-logo.png')}}"
                                                            title="صنعتی بهشهر" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="24"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide024"><img
                                                            alt="طبیعت سبز میهن" src="{{ asset('images/frontend_images/home/tabiat-logo.png')}}"
                                                            title="طبیعت سبز میهن" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="25"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide025"><img
                                                            alt="گلبهار جاده ابریشم "
                                                            src="{{ asset('images/frontend_images/home/golbahar-logo.png')}}"
                                                            title="گلبهار جاده ابریشم" width="75" height="75">
                                                </div>
                                                <div class="slick-slide" data-slick-index="26"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide026"><a
                                                            href="#/?p=10305" target="_blank" tabindex="-1"><img
                                                                alt="مه کامه" src="{{ asset('images/frontend_images/home/mahkaame-logo.png')}}"
                                                                title="مه کامه" width="75" height="75"></a></div>
                                                <div class="slick-slide" data-slick-index="27"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide027"><img
                                                            alt="نوش آذر" src="{{ asset('images/frontend_images/home/noshazar-logo.png')}}"
                                                            title="نوش آذر" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="28"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide028"><img
                                                            alt="زرین گل رایکا" src="{{ asset('images/frontend_images/home/hana-logo.png')}}"
                                                            title="زرین گل رایکا" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="29"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide029"><img
                                                            alt="عالیا گلستان" src="{{ asset('images/frontend_images/home/aliya-logo.png')}}"
                                                            title="عالیا گلستان" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="30"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide030"><img
                                                            alt="شادگل نیشابور " src="{{ asset('images/frontend_images/home/shadgol-logo.png')}}"
                                                            title="شادگل نیشابور" width="75" height="54"></div>
                                                <div class="slick-slide" data-slick-index="31"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide031"><a
                                                            href="#/?p=10593" target="_blank" tabindex="-1"><img
                                                                alt="ارجان نوین" src="{{ asset('images/frontend_images/home/arjan-logo.png')}}"
                                                                title="ارجان نوین" width="75" height="75"></a></div>
                                                <div class="slick-slide" data-slick-index="32"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide032"><img
                                                            alt=" تدارک گستر ایرانیان"
                                                            src="{{ asset('images/frontend_images/home/tgi-logo.png')}}"
                                                            title="تدارک گستر ایرانیان" width="75" height="75">
                                                </div>
                                                <div class="slick-slide" data-slick-index="33"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide033"><img
                                                            alt="شیرین عسل " src="{{ asset('images/frontend_images/home/shirinasal-logo.png')}}"
                                                            title="شیرین عسل" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="34"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide034"><img
                                                            alt="سبزنگار البرز" src="{{ asset('images/frontend_images/home/alborz-logo.png')}}"
                                                            title="سبزنگار البرز" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="35"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide035"><img
                                                            alt="تجارت گستران خوارزمی"
                                                            src="{{ asset('images/frontend_images/home/tgk-logo.png')}}"
                                                            title="تجارت گستران خوارزمی" width="75" height="75">
                                                </div>
                                                <div class="slick-slide" data-slick-index="36"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide036"><img
                                                            alt=" روغن نباتي جهان" src="{{ asset('images/frontend_images/home/jahan-logo.png')}}"
                                                            title="روغن نباتی جهان" width="75" height="75">
                                                </div>
                                                <div class="slick-slide" data-slick-index="37"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide037"><img
                                                            alt="تبرک" src="{{ asset('images/frontend_images/home/tabarok-logo.png')}}"
                                                            title="تبرک" width="75" height="35"></div>
                                                <div class="slick-slide" data-slick-index="38"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide038"><img
                                                            alt="نرگس شیراز" src="{{ asset('images/frontend_images/home/narges-logo.png')}}"
                                                            title="نرگس شیراز" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="39"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide039"><img
                                                            alt="گلستان دزفول" src="{{ asset('images/frontend_images/home/dezful-logo.png')}}"
                                                            title="گلستان دزفول" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="40"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide040"><img
                                                            alt="نوشدارو دریا " src="{{ asset('images/frontend_images/home/darya-logo.png')}}"
                                                            title="نوشدارو دریا" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="41"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide041"><img
                                                            alt="گلوکزان" src="{{ asset('images/frontend_images/home/glocoszan-logo.png')}}"
                                                            title="گلوکزان" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="42"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide042"><img
                                                            alt="سوربن شمال" src="{{ asset('images/frontend_images/home/sorbon-logo.png')}}"
                                                            title="سوربن شمال" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="43"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide043"><img
                                                            alt="کشت و صنعت شمال"
                                                            src="{{ asset('images/frontend_images/home/ks-shomal-logo.png')}}"
                                                            title="کشت و صنعت شمال" width="75" height="75">
                                                </div>
                                                <div class="slick-slide" data-slick-index="44"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide044"><img
                                                            alt="پرتو دانه خزر" src="{{ asset('images/frontend_images/home/parto-logo.png')}}"
                                                            title="پرتو دانه خزر" width="75" height="48"></div>
                                                <div class="slick-slide" data-slick-index="45"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide045"><img
                                                            alt="خاوردشت" src="{{ asset('images/frontend_images/home/khavar-logo.png')}}"
                                                            title="خاوردشت" width="63" height="75"></div>
                                                <div class="slick-slide" data-slick-index="46"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide046"><a
                                                            href="#/?p=9738" target="_blank" tabindex="-1"><img
                                                                alt="توسعه مهرونداد"
                                                                src="{{ asset('images/frontend_images/home/mehrvandad-logo.png')}}"
                                                                title="توسعه مهرونداد" width="75" height="75"></a>
                                                </div>
                                                <div class="slick-slide" data-slick-index="47"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide047"><img
                                                            alt="فریکو" src="{{ asset('images/frontend_images/home/frico-logo.png')}}"
                                                            title="فریکو" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="48"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide048"><img
                                                            alt="پارس ماهان آسیا" src="{{ asset('images/frontend_images/home/mahan-logo.jpg')}}"
                                                            title="پارس ماهان آسیا" width="75" height="75">
                                                </div>
                                                <div class="slick-slide" data-slick-index="49"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide049"><img
                                                            alt="زیتون طلایی"
                                                            src="{{ asset('images/frontend_images/home/zeytoontalaei-logo.png')}}"
                                                            title="زیتون طلایی" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="50"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide050"><img
                                                            alt="چیلک شرقی" src="{{ asset('images/frontend_images/home/chilak-logo.png')}}"
                                                            title="چیلک شرقی" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="51"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide051"><img
                                                            alt="یگانه خزر" src="{{ asset('images/frontend_images/home/yeganeh-logo.png')}}"
                                                            title="یگانه خزر" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="52"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide052"><img
                                                            alt="سبوس مازند" src="{{ asset('images/frontend_images/home/sabos-logo.png')}}"
                                                            title="سبوس مازند" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="53"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide053"><img
                                                            alt="كنجد سمن" src="{{ asset('images/frontend_images/home/konjedsaman-logo.png')}}"
                                                            title="کنجد سمن" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="54"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide054"><img
                                                            alt="خرمشهر" src="{{ asset('images/frontend_images/home/khoramshahr-logo.png')}}"
                                                            title="خرمشهر" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="55"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide055"><a
                                                            href="#/?p=10661" target="_blank" tabindex="-1"><img
                                                                alt="آرین طعم خزر " src="{{ asset('images/frontend_images/home/vioni-logo.png')}}"
                                                                title="آرین طعم خزر" width="75" height="75"></a>
                                                </div>
                                                <div class="slick-slide" data-slick-index="56"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide056"><a
                                                            href="#/?p=7752" target="_blank" tabindex="-1"><img
                                                                alt="گلبرگ بهاران" src="{{ asset('images/frontend_images/home/golbarg-logo.png')}}"
                                                                title="گلبرگ بهاران" width="75" height="75"></a>
                                                </div>
                                                <div class="slick-slide slick-current slick-active"
                                                     data-slick-index="57" aria-hidden="false"
                                                     style="width: 109px;" tabindex="-1" role="option"
                                                     aria-describedby="slick-slide057"><a href="#/?p=10761"
                                                                                          target="_blank"
                                                                                          tabindex="0"><img
                                                                alt="نوین گستر مشکات"
                                                                src="{{ asset('images/frontend_images/home/Untitled-1-1-150x150.jpg')}}"
                                                                title="نوین گستر مشکات" width="75" height="75"></a>
                                                </div>
                                                <div class="slick-slide slick-active" data-slick-index="58"
                                                     aria-hidden="false" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide058"><img
                                                            alt="حیات هرمزگان" src="{{ asset('images/frontend_images/home/hayat-logo.png')}}"
                                                            title="حیات هرمزگان" width="75" height="75"></div>
                                                <div class="slick-slide slick-active" data-slick-index="59"
                                                     aria-hidden="false" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide059"><img
                                                            alt="ماهیدشت کرمانشاه"
                                                            src="{{ asset('images/frontend_images/home/mahidasht-logo.png')}}"
                                                            title="ماهیدشت کرمانشاه" width="75" height="75">
                                                </div>
                                                <div class="slick-slide slick-active" data-slick-index="60"
                                                     aria-hidden="false" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide060"><a
                                                            href="#/?p=9758" target="_blank" tabindex="0"><img
                                                                alt="گلناز کرمان" src="{{ asset('images/frontend_images/home/golnaz-logo.png')}}"
                                                                title="گلناز کرمان" width="75" height="42"></a>
                                                </div>
                                                <div class="slick-slide slick-active" data-slick-index="61"
                                                     aria-hidden="false" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide061"><img
                                                            alt="گل سفید البرز" src="{{ asset('images/frontend_images/home/golsefid-logo.png')}}"
                                                            title="گل سفید البرز" width="75" height="75"></div>
                                                <div class="slick-slide slick-active" data-slick-index="62"
                                                     aria-hidden="false" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide062"><a
                                                            href="#/?p=11577" target="_blank" tabindex="0"><img
                                                                alt="روغن نباتی اردبیل"
                                                                src="{{ asset('images/frontend_images/home/ardebil-logo.png')}}"
                                                                title="روغن نباتی اردبیل" width="75"
                                                                height="75"></a></div>
                                                <div class="slick-slide slick-active" data-slick-index="63"
                                                     aria-hidden="false" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide063"><img
                                                            alt="صنعتی بهپاک" src="{{ asset('images/frontend_images/home/behpak-logo.png')}}"
                                                            title="صنعتی بهپاک" width="75" height="75"></div>
                                                <div class="slick-slide slick-active" data-slick-index="64"
                                                     aria-hidden="false" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide064"><img
                                                            alt=" ناب دانه سمنان" src="{{ asset('images/frontend_images/home/nabdane-logo.png')}}"
                                                            title="ناب دانه سمنان" width="75" height="75"></div>
                                                <div class="slick-slide slick-active" data-slick-index="65"
                                                     aria-hidden="false" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide065"><a
                                                            href="#/?p=10389" target="_blank" tabindex="0"><img
                                                                alt="گیتا طلایی گلستان" src="{{ asset('images/frontend_images/home/giti-logo.png')}}"
                                                                title="گیتا طلایی گلستان" width="75"
                                                                height="43"></a></div>
                                                <div class="slick-slide slick-active" data-slick-index="66"
                                                     aria-hidden="false" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide066"><a
                                                            href="#/?p=10951" target="_blank" tabindex="0"><img
                                                                alt="مارگارین" src="{{ asset('images/frontend_images/home/margarin-logo.png')}}"
                                                                title="مارگارین" width="65" height="75"></a></div>
                                                <div class="slick-slide" data-slick-index="67"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide067"><img
                                                            alt="طلای سفید گنبد" src="{{ asset('images/frontend_images/home/talsefid-logo.png')}}"
                                                            title="طلای سفید گنبد" width="75" height="75"></div>
                                                <div class="slick-slide" data-slick-index="68"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide068"><img
                                                            alt="فضل نیشابور" src="{{ asset('images/frontend_images/home/fazl-logo.jpg')}}"
                                                            title="فضل نیشابور" width="75" height="53"></div>
                                                <div class="slick-slide" data-slick-index="69"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide069"><a
                                                            href="#/?p=12965" target="_blank" tabindex="-1"><img
                                                                alt="شرکت فرآوری سبوس کندوج"
                                                                src="{{ asset('images/frontend_images/home/kandooj-2-150x150.jpg')}}"
                                                                title="فرآوری سبوس کندوج" width="75"
                                                                height="75"></a></div>
                                                <div class="slick-slide" data-slick-index="70"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide070"><a
                                                            href="#/?p=13364" target="_blank" tabindex="-1"><img
                                                                alt="شرکت روغن نباتی ناز اصفهان"
                                                                src="{{ asset('images/frontend_images/home/0-5307-150x150.jpg')}}"
                                                                title="شرکت روغن نباتی ناز اصفهان" width="75"
                                                                height="75"></a></div>
                                                <div class="slick-slide" data-slick-index="71"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide071"><a
                                                            href="#/?p=13039" target="_blank" tabindex="-1"><img
                                                                alt="" src="{{ asset('images/frontend_images/home/0-5267-150x150.jpg')}}" title=""
                                                                width="75" height="75"></a></div>
                                                <div class="slick-slide" data-slick-index="72"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide072"><a
                                                            href="#/?p=13314" target="_blank" tabindex="-1"><img
                                                                alt="شرکت گلبهار سپاهان"
                                                                src="{{ asset('images/frontend_images/home/0-5303-150x150.jpg')}}"
                                                                title="گلبهار سپاهان" width="75" height="75"></a>
                                                </div>
                                                <div class="slick-slide" data-slick-index="73"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide073"><img
                                                            alt="" src="{{ asset('images/frontend_images/home/logo-150x150.gif')}}"
                                                            title="شرکت زیتون طلایی" width="75" height="75">
                                                </div>
                                                <div class="slick-slide" data-slick-index="74"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide074"><a
                                                            href="#/?p=12642" target="_blank" tabindex="-1"><img
                                                                alt="شرکت گلبهار پارسیان"
                                                                src="{{ asset('images/frontend_images/home/LOGO-GOLBAHARPARSIAN-whit-150x150.jpg')}}"
                                                                title="شرکت گلبهار پارسیان" width="75" height="75"></a>
                                                </div>
                                                <div class="slick-slide" data-slick-index="75"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide075"><a
                                                            href="#/?p=11525" target="_blank" tabindex="-1"><img
                                                                alt="شرکت گلستان" src="{{ asset('images/frontend_images/home/0-3461-150x118.jpg')}}"
                                                                title="شرکت گلستان" width="75" height="59"></a>
                                                </div>
                                                <div class="slick-slide" data-slick-index="76"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide076"><a
                                                            href="#/?p=11118" target="_blank" tabindex="-1"><img
                                                                alt="لابراتوار داروئی و غذائی بالک"
                                                                src="{{ asset('images/frontend_images/home/verina-logo.png')}}"
                                                                title="لابراتوار داروئی و غذائی بالک" width="75"
                                                                height="75"></a></div>
                                                <div class="slick-slide" data-slick-index="77"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide077"><a
                                                            href="#/?p=10386" target="_blank" tabindex="-1"><img
                                                                alt="مجتمع مخازن روغنی خلیج فارس"
                                                                src="{{ asset('images/frontend_images/home/persian-gulf-logo.png')}}"
                                                                title="مجتمع مخازن روغنی خلیج فارس" width="52"
                                                                height="75"></a></div>
                                                <div class="slick-slide" data-slick-index="78"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide078"><img
                                                            alt="آسیا فراز ایرانیان "
                                                            src="{{ asset('images/frontend_images/home/asia-logo.png')}}"
                                                            title="آسیا فراز ایرانیان" width="75" height="75">
                                                </div>
                                                <div class="slick-slide" data-slick-index="79"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1"
                                                     role="option" aria-describedby="slick-slide079"><a
                                                            href="#/?p=10328" target="_blank" tabindex="-1"><img
                                                                alt="آلتن دانه صحرا" src="{{ asset('images/frontend_images/home/bayragh-logo.png')}}"
                                                                title="آلتن دانه صحرا" width="75" height="75"></a>
                                                </div>
                                                <div class="slick-slide slick-cloned" data-slick-index="80"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1">
                                                    <img alt="حیات هرمزگان" src="{{ asset('images/frontend_images/home/hayat-logo.png')}}"
                                                         title="حیات هرمزگان" width="75" height="75"></div>
                                                <div class="slick-slide slick-cloned" data-slick-index="81"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1">
                                                    <img alt="ماهیدشت کرمانشاه"
                                                         src="{{ asset('images/frontend_images/home/mahidasht-logo.png')}}"
                                                         title="ماهیدشت کرمانشاه" width="75" height="75">
                                                </div>
                                                <div class="slick-slide slick-cloned" data-slick-index="82"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1">
                                                    <a href="#/?p=9758" target="_blank" tabindex="-1"><img
                                                                alt="گلناز کرمان" src="{{ asset('images/frontend_images/home/golnaz-logo.png')}}"
                                                                title="گلناز کرمان" width="75" height="42"></a>
                                                </div>
                                                <div class="slick-slide slick-cloned" data-slick-index="83"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1">
                                                    <img alt="گل سفید البرز" src="{{ asset('images/frontend_images/home/golsefid-logo.png')}}"
                                                         title="گل سفید البرز" width="75" height="75"></div>
                                                <div class="slick-slide slick-cloned" data-slick-index="84"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1">
                                                    <a href="#/?p=11577" target="_blank" tabindex="-1"><img
                                                                alt="روغن نباتی اردبیل"
                                                                src="{{ asset('images/frontend_images/home/ardebil-logo.png')}}"
                                                                title="روغن نباتی اردبیل" width="75"
                                                                height="75"></a></div>
                                                <div class="slick-slide slick-cloned" data-slick-index="85"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1">
                                                    <img alt="صنعتی بهپاک" src="{{ asset('images/frontend_images/home/behpak-logo.png')}}"
                                                         title="صنعتی بهپاک" width="75" height="75"></div>
                                                <div class="slick-slide slick-cloned" data-slick-index="86"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1">
                                                    <img alt=" ناب دانه سمنان"
                                                         src="{{ asset('images/frontend_images/home/nabdane-logo.png')}}"
                                                         title="ناب دانه سمنان" width="75" height="75">
                                                </div>
                                                <div class="slick-slide slick-cloned" data-slick-index="87"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1">
                                                    <a href="#/?p=10389" target="_blank" tabindex="-1"><img
                                                                alt="گیتا طلایی گلستان"
                                                                src="{{ asset('images/frontend_images/home/giti-logo.png')}}"
                                                                title="گیتا طلایی گلستان" width="75"
                                                                height="43"></a></div>
                                                <div class="slick-slide slick-cloned" data-slick-index="88"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1">
                                                    <a href="#/?p=10951" target="_blank" tabindex="-1"><img
                                                                alt="مارگارین" src="{{ asset('images/frontend_images/home/margarin-logo.png')}}"
                                                                title="مارگارین" width="65" height="75"></a>
                                                </div>
                                                <div class="slick-slide slick-cloned" data-slick-index="89"
                                                     aria-hidden="true" style="width: 109px;" tabindex="-1">
                                                    <img alt="طلای سفید گنبد"
                                                         src="{{ asset('images/frontend_images/home/talsefid-logo.png')}}"
                                                         title="طلای سفید گنبد" width="75" height="75">
                                                </div>
                                            </div>
                                        </div>


                                        <button type="button" data-role="none"
                                                class="slick-next slick-arrow" aria-label="Next"
                                                role="button" style="display: block;">Next
                                        </button>
                                    </div>
                                </div>


                            </div>
                            <div id="ctl30_iFooterPanel" class="SBFooterPanels"
                                 style="text-align:left;border-radius:0px 0px -1px -1px;">

                                <span id="ctl30_iFooterTextToView"></span>

                            </div>

                        </div>


                    </div>
                    <div class="clearAlign"></div>
                </div>
            </div>
            <div id="LastSideZoneBackgroundTD"
                 class="leftalign col4 LastSideZoneBackgroundTD DisplayBlock initialTextAlign">


            </div>
            <div class="clearAlign"></div>
        </div>
    </div>
@endsection