<div id="OuterInlineContentsControl">
    <div id="InlineContentsControl" class="clearAlign  ">
        <div id="FirstSideZoneBackgroundTD"
             class="leftalign col4 FirstSideZoneBackgroundTD DisplayBlock initialTextAlign">

        </div>
        <div id="TemplateHeader2ItemParent" class="leftalign col2 DisplayBlock initialTextAlign">
            <div id="MiddleContentsControl" class="clearAlign">
                <div class="TemplateHeader2BackgroundTD" id="TH2ContentsControl">

                </div>
                <div id="PageContainer">

                    <div id="HeaderContainer" class="headerBackgroundTD ">


                        <div id="IMHC3" class="leftalign col4 DisplayBlock">

                            <div id="mystyle" class="SBContainerPanels"
                                 style="border-color:#f0f0f0;border-width:1px;border-style:Solid;-webkit-box-shadow:0px 0px 0px #777;-webkit-border-radius:0px;width:100%;-moz-border-radius:0px;box-shadow:0px 0px 0px #777;margin-left:0px;background-color:white;margin-bottom:7px;border-radius:0px;margin-right:0px;max-width:800px;margin-top:7px;">

                                <div id="ctl05_iHeaderPanel" class="SBHeaderPanels"
                                     style="text-align:right;background-color:#222;padding-top:7px;padding-left:0px;padding-right:12px;border-radius:-1px -1px 0px 0px;">

                                    <span id="ctl05_iHeaderTextToView" class="aspNetDisabled" style="color:#fff;"><h2>آیا روغن نارگیل به کاهش وزن کمک می‌کند؟</h2></span>

                                </div>
                                <div id="ctl05_iContentPanel" class="SBContentPanels"
                                     style="padding-top:10px;padding-left:15px;padding-right:15px;padding-bottom:15px;">

                                    <div class="NewsSubTitle">اخیراً مصرف روغن نارگیل به شکل‌های مختلف بسیار
                                        رواج یافته است. اما افراد کمتری می‌دانند که 90 درصد چربی این روغن از
                                        نوع چربی اشباع شده است که باعث افزایش کلسترول و خطر ابتلا به بیماری‌های
                                        قلبی می‌شود.
                                    </div>
                                    <div style="width:100%" class="NewsBodyContents">
                                        <div id="ToPrint" class="NewsItemTextToShow" style="overflow: hidden;"><a
                                                    class="tiptip"
                                                    href="http://www.ivoia.com/files/ivoia-com/NewsSystem/5-28-2019/%D8%B1%D9%88%D8%BA%D9%86%20%D9%86%D8%A7%D8%B1%DA%AF%DB%8C%D9%84.jpg"
                                                    target="_blank"><img alt="آیا روغن نارگیل به کاهش وزن کمک می‌کند؟"
                                                                         src="%D8%A2%DB%8C%D8%A7%20%D8%B1%D9%88%D8%BA%D9%86%20%D9%86%D8%A7%D8%B1%DA%AF%DB%8C%D9%84%20%D8%A8%D9%87%20%DA%A9%D8%A7%D9%87%D8%B4%20%D9%88%D8%B2%D9%86%20%DA%A9%D9%85%DA%A9%20%D9%85%DB%8C%E2%80%8C%DA%A9%D9%86%D8%AF%D8%9F_files/ThumbnailImage.jpg"
                                                                         class="NewsImgs NewsDetailImage"
                                                                         style="margin:10px;max-width:400px;width:100%;float:right;"></a>
                                            <p align="justify"></p>
                                            <p style="text-align: justify;"><span style="font-size:14px;"><span
                                                            style="font-family:Tahoma,Geneva,sans-serif;">به
 گزارش روابط عمومی انجمن صنایع روغن نباتی ایران به نقل از سلامت نیوز،
برخی محققان حتی معتقدند از آنجایی که روغن نارگیل چربی بد (ال دی ال) بدن
را بالا می‌برد باید از مصرف آن پرهیز کرد، اما برخی متخصصان تغذیه معتقدند
 این ماده برای کاهش وزن بسیار مؤثر است.<br>
<br>
از بین بردن چربی شکم<br>
<br>
برخی تحقیقات نشان داده‌اند روغن نارگیل برای سوختن چربی، بویژه چربی دور
شکم که برای بیماری‌های قلبی خطرناک است مؤثر است. در بین دو گروه بانوان
که برای یک ماه روزانه 40 گرم روغن دانه سویا و روغن نارگیل مصرف کرده
بودند، تنها گروه دوم چربی شکم را از دست داده‌اند. با این حال، مصرف 40
گرم اسید چرب اشباع شده، کاری بی‌خطر نیست. بنابراین، بهتر است مصرف زیاد
روغن نارگیل برای کاهش وزن را کنار بگذارید و آن را تنها در 5 تا 6 درصد
کالری مصرفی در روز خود جای دهید.<br>
<br>
سیر نگه داشتن فرد<br>
<br>
چربی‌ها به‌طور کلی فرد را برای مدتی طولانی‌تر سیر نگه می‌دارند. از این
منظر، روغن نارگیل نیز انتخاب مناسبی به نظر می‌آید. تحقیقات نشان داده است
 مصرف روزانه مقدار کمی از آن به بالا رفتن چربی خوب (اچ دی ال) خون کمک
می‌کند. اما برای اینکه روغن نارگیل مانع از گرسنگی در طول روز شود، باید
روزانه حداقل 30 گرم، یعنی حدود دو برابر میزان مجاز مصرف چربی اشباع شده
(13 گرم در روز) از آن مصرف کرد. این کار کلسترول را بالا خواهد برد.<br>
<br>
افزایش متابولیسم<br>
<br>
چربی روغن نارگیل با چربی روغن‌های دیگر مثل روغن زیتون، روغن کانولا، روغن
 آواکادو و روغن دانه سویا ساختاری متفاوت دارد. این بدان معناست که پروسه
هضم این روغن متفاوت است و در واقع با گلوکز رقابت می‌کند تا منبع اصلی
تأمین انرژی بدن باشد و بدین ترتیب باعث بالا رفتن متابولیسم می‌شود. </span></span></p>

                                            <p style="text-align: justify;"><span style="font-size:14px;"><span
                                                            style="font-family:Tahoma,Geneva,sans-serif;">بدین ترتیب بدن شما با مصرف این روغن کالری بیشتری می‌سوزاند که انرژی سلول‌های بدن را تا 24 ساعت تأمین می‌کند. </span></span>
                                            </p>

                                            <p style="text-align: justify;"><span style="font-size:14px;"><span
                                                            style="font-family:Tahoma,Geneva,sans-serif;">اما
 برای اینکه روغن نارگیل تأثیر چشمگیری در افزایش متابولیسم داشته باشد
باید روزانه 80 گرم از آن را مصرف کرد. مصرف این حجم از هیچ نوع روغنی،
بخصوص روغن‌های سرشار از چربی اشباع شده به هیچ عنوان توصیه نمی‌شود.</span></span></p>
                                            <p></p></div>
                                        <div class="NewssystemInfo"><p class="NewssystemId">کد: 50018882</p>
                                            <p class="NewssystemPublishTime">زمان انتشار: سه شنبه 7 خرداد 1398 01:26
                                                ب.ظ</p>
                                            <p class="NewssystemVisitCount">تعداد نمایش: 76</p></div>
                                        <div>
                                            <img src="%D8%A2%DB%8C%D8%A7%20%D8%B1%D9%88%D8%BA%D9%86%20%D9%86%D8%A7%D8%B1%DA%AF%DB%8C%D9%84%20%D8%A8%D9%87%20%DA%A9%D8%A7%D9%87%D8%B4%20%D9%88%D8%B2%D9%86%20%DA%A9%D9%85%DA%A9%20%D9%85%DB%8C%E2%80%8C%DA%A9%D9%86%D8%AF%D8%9F_files/print.gif"
                                                 style="cursor:pointer"
                                                 onclick="printarea('','ToPrint','','<h2>آیا روغن نارگیل به کاهش وزن کمک می‌کند؟</h2>اخیراً مصرف روغن نارگیل به شکل‌های مختلف بسیار رواج یافته است. اما افراد کمتری می‌دانند که 90 درصد چربی این روغن از نوع چربی اشباع شده است که باعث افزایش کلسترول و خطر ابتلا به بیماری‌های قلبی می‌شود.','<p>زمان انتشار: سه شنبه 7 خرداد 1398 01:26 ب.ظ</p>');">
                                        </div>
                                    </div>
                                </div>
                                <div id="ctl05_iFooterPanel" class="SBFooterPanels"
                                     style="text-align:left;border-radius:0px 0px -1px -1px;">

                                    <span id="ctl05_iFooterTextToView"></span>

                                </div>

                            </div>
                        </div>
                        <div id="IMHC4" class="leftalign col4 DisplayBlock">
                            <div id="ctl08_iViewMode" class="SBContainerPanels"
                                 style="border-color:#f0f0f0;border-width:1px;border-style:Solid;-webkit-box-shadow:0px 0px 0px #777;-webkit-border-radius:0px;-moz-border-radius:0px;box-shadow:0px 0px 0px #777;margin-left:0px;background-color:#ffffff;margin-bottom:7px;border-radius:0px;margin-right:0px;margin-top:7px;">

                                <div id="ctl08_iHeaderPanel" class="SBHeaderPanels"
                                     style="text-align:center;padding-top:0px;padding-left:0px;padding-right:0px;border-radius:-1px -1px 0px 0px;">

                                    <span id="ctl08_iHeaderTextToView" class="aspNetDisabled"><h4> آخرین شماره ماهنامه آفتابگردان</h4></span>

                                </div>
                                <div id="ctl08_iContentPanel" class="SBContentPanels"
                                     style="padding-top:7px;padding-left:7px;padding-right:7px;padding-bottom:7px;">

                                    <p style="text-align: center !important;"><img alt=""
                                                                                   src="%D8%A2%DB%8C%D8%A7%20%D8%B1%D9%88%D8%BA%D9%86%20%D9%86%D8%A7%D8%B1%DA%AF%DB%8C%D9%84%20%D8%A8%D9%87%20%DA%A9%D8%A7%D9%87%D8%B4%20%D9%88%D8%B2%D9%86%20%DA%A9%D9%85%DA%A9%20%D9%85%DB%8C%E2%80%8C%DA%A9%D9%86%D8%AF%D8%9F_files/1-76-1.jpg"
                                                                                   width="72" height="99"></p>
                                </div>
                                <div id="ctl08_iFooterPanel" class="SBFooterPanels"
                                     style="text-align:left;border-radius:0px 0px -1px -1px;">

                                    <span id="ctl08_iFooterTextToView"></span>

                                </div>

                            </div>
                        </div>
                        <div class="clearAlign"></div>

                    </div>
                </div>
                <div id="TF2ContentsControl" class="TemplateFooter2BackgroundTD">

                </div>
                <div class="clearAlign"></div>
            </div>
        </div>
        <div id="LastSideZoneBackgroundTD"
             class="leftalign col4 LastSideZoneBackgroundTD DisplayBlock initialTextAlign">
        </div>
        <div class="clearAlign"></div>
    </div>
</div>
