<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
    <ul>
        <li class="active"><a href="{{ url('/admin/dashboard')}}"><i class="icon icon-home"></i> <span>پیشخوان</span></a> </li>
        </li>
        <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>پست ها</span> <span class="label label-important">4</span></a>
            <ul>
                <li><a href="{{ url('/admin/view-products')}}">همه پست ها</a></li>
                <li><a href="{{ url('/admin/add-product') }}">پست جدید</a></li>
                <li><a href="{{ url('/admin/view-categories') }}">دسته بندی ها</a></li>
                <li><a href="{{ url('/admin/add-category') }}">دسته بندی جدید</a></li>
            </ul>
        </li>
        <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>فهرست ها</span> <span class="label label-important">4</span></a>
            <ul>
                <li><a href="{{ url('/admin/edit-about')}}">درباره انجمن</a></li>
                <li><a href="{{ url('/admin/edit-member') }}">اعضای انجمن</a></li>
                <li><a href="{{ url('/admin/edit-gallery') }}">گالری</a></li>
                <li><a href="{{ url('/admin/edit-contacts') }}">تماس با ما</a></li>
            </ul>
        </li>
        <li class="submenu"> <a href="#"><i class="icon icon-file"></i> <span>اسلایدر ها</span> <span class="label label-important">2</span></a>
            <ul>
                <li><a href="{{ url('/admin/add-banner') }}">اسلایدر جدید</a></li>
                <li><a href="{{ url('/admin/view-banners') }}">انمایش اسلایدر</a></li>
            </ul>
        </li>
    </ul>
</div>
<!--sidebar-menu-->