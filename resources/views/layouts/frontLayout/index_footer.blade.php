<div id="TF1ContentsControl" class="clearAlign TemplateFooter1BackgroundTD">
    <div id="footerDiv" class="SBContainerPanels"
         style="border-color:;border-width:0px;border-style:None;-webkit-box-shadow:0px 0px 0px #777;-webkit-border-radius:0px;width:100%;-moz-border-radius:0px;box-shadow:0px 0px 0px #777;margin-left:0px;background-color:#222;margin-bottom:0px;border-radius:0px;margin-right:0px;margin-top:5px;">

        <div id="ctl31_iHeaderPanel" class="SBHeaderPanels"
             style="height:4px;text-align:left;background-color:#fbaa38;padding-top:0px;padding-left:0px;padding-right:0px;border-radius:0px 0px 0px 0px;">

            <span id="ctl31_iHeaderTextToView" class="aspNetDisabled"></span>

        </div>
        <div id="ctl31_iContentPanel" class="SBContentPanels"
             style="padding-top:0px;padding-left:0px;padding-right:0px;padding-bottom:0px;">

            <div id="footerInnerDiv">
                <div class="ft1" style="text-align: justify;padding-top: 10px;font-size: 8.5pt;" cellspacing="3" cellpadding="3" border="0" align="center">
                    <div>
                        <img src="{{ asset('images/frontend_images/home/iaf.png')}}"/ width="300px">

                        <p><span style="color:#ffffff;"><i class="fa fa-map-signs"></i> آدرس:&nbsp;ایران، تهران، خیابان گاندی، خیابان دوم، پلاک 6، طبقه اول، واحد 12</span>
                        </p>

                        <p><span style="color:#ffffff;"><i class="fa fa-phone-square"></i> 02188661433</span></p>

                        <p><span style="color:#ffffff;"><i class="fa fa-fax"></i> 02188661433</span></p>

                        <p>&nbsp;</p>

                        <p>&nbsp;</p>
                    </div>
                </div>
                <div class="ft1" style="text-align: justify;padding-top: 10px;font-size: 8.5pt;" cellspacing="3" cellpadding="3" border="0" align="center">
                    <div>

                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d809.532656413788!2d51.412226829247295!3d35.747593998761175!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzXCsDQ0JzUxLjMiTiA1McKwMjQnNDYuMCJF!5e0!3m2!1sen!2s!4v1573394110830!5m2!1sen!2s" width="400" height="250" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </div>
                </div>
                <div class="ft2" style="text-align: justify; color: #fff; padding-top: 10px; text-align: center;font-size: 15px; width: 25%; float: right;" cellspacing="3" cellpadding="3" border="0" align="center">
                    <div>
                        <span style="text-align:center;">لینک های مفید</span>
                        <ul style="list-style-type: none;">
                            <li>رئیس جمهوری</li>
                            <li>رئیس جمهوری</li>
                            <li>رئیس جمهوری</li>
                            <li>رئیس جمهوری</li>
                            <li>رئیس جمهوری</li>
                            <li>رئیس جمهوری</li>
                        </ul>
                    </div>
                </div>
                <div class="ft3"  cellspacing="3" cellpadding="3" border="0" align="center">
                    <div>
                        <img src="{{ asset('images/frontend_images/home/tlm.png')}}"/ width="150px">
                    </div>
                </div>
                <div style="width: 100%;clear: both; display: block ruby;">
                    <p style="width: 100%; text-align:left;"><span style="color:#ffffff;">تمامی حقوق مادی و معنوی این سایت متعلق بع اتحادیه وارد کنندگان نهاده های دام و طیور ایران می باشد</span></p>
                    <p style="text-align:right;"><span style="color:#ffffff;">طراحی شده توسط تیم <a href="https://jahansite.com/"><img src="https://jahansite.com/wp-content/themes/hezb/includes/images/nav_logo.png"/></a></span></p></div>
            </div>
        </div>
    </div>
</div>