<form method="post" action="/" id="form1" enctype="multipart/form-data">
    <div class="aspNetHidden">
        <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="">
        <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="">
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE"
               value="un0EBHuGJvT5BuYAnBxCfR87ZM2nKcY0QKjAyAOAf6ntOScHX6H/j1m1Q19/zgfLGvcX6JhIC4ZvSfKd4m5uhUjS394CXvXpRUfoJVZO5vUKQhFkjqbWLRRCw/U=">
    </div>
    <div id="ChartContainer"
         style="direction:ltr;z-index:100;border: 3px solid  #542416;display:none;position:fixed;background-image: url(/images/50transparent.png); border-radius:5px;">
        <table style="height:120px;background-image: url(/images/50transparent.png);" cellspacing="0" cellpadding="2">
            <tbody>
            <tr>
                <td valign="bottom">
                    <table style="height:120px" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                        <tr>
                            <td id="Max" valign="top"></td>
                        </tr>
                        <tr>
                            <td id="Mid"></td>
                        </tr>
                        <tr>
                            <td id="Min" valign="bottom"></td>
                        </tr>
                        </tbody>
                    </table>
                </td>
                <td valign="middle">
                    <table id="ChartData" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                        <tr>
                            <td colspan="11">
                                <table cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                    <tr>
                                        <td class="ChartMargin"></td>
                                        <td>
                                            <div id="Chart"
                                                 style="border-left:1px solid black;border-right:1px solid black;border-bottom:1px solid black;margin:0px;padding:0px;"></div>
                                        </td>
                                        <td class="ChartMargin"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="aspNetHidden">

        <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AE6095DE">
    </div>
    <div>
        <script type="text/javascript">
            //<![CDATA[
            Sys.WebForms.PageRequestManager._initialize('ScriptManager1', 'form1', ['tUpdatePanel1', 'UpdatePanel1'], [], [], 90, '');
            //]]>
        </script>


        <div id="UpdatePanel1">


            <div id="PageControl">
                <div id="TH1ContentsControl" class="clearAlign TemplateHeader1BackgroundTD ">


                    <div id="ctl23_iViewMode" class="SBContainerPanels"
                         style="border-color:;border-width:0px;border-style:None;-webkit-box-shadow:0px 0px 0px #777;-webkit-border-radius:0px;-moz-border-radius:0px;box-shadow:0px 0px 0px #777;margin-left:0px;margin-bottom:0px;border-radius:0px;margin-right:0px;margin-top:0px;">


                        <div id="ctl23_iContentPanel"
                             style="padding-top:0px;padding-left:0px;padding-right:0px;padding-bottom:0px;">
                        </div>


                    </div>

                    <div id="HeadThin" class="SBContainerPanels"
                         style="border-width: 0px; border-style: none; width: 100%; box-shadow: rgb(119, 119, 119) 0px 0px 0px; margin: 0px; background-color: rgb(34, 34, 34); border-radius: 0px; display: block;">

                        <div id="ctl24_iHeaderPanel" class="SBHeaderPanels"
                             style="text-align:left;padding-top:0px;padding-left:0px;padding-right:0px;border-radius:0px 0px 0px 0px;">

                            <span id="ctl24_iHeaderTextToView" class="aspNetDisabled"></span>

                        </div>
                        <div id="ctl24_iContentPanel" class="SBContentPanels"
                             style="padding-top:2px;padding-left:0px;padding-right:0px;padding-bottom:2px;">

                            <table style="width:97%;max-width:1100px;" cellspacing="1" cellpadding="1" border="0"
                                   align="center">
                                <tbody>
                                <tr>
                                    <td style="width: 25px;"><a href="#"><img alt="" id="lanicon"
                                                                              src="{{ asset('images/frontend_images/home/fen.png')}}"
                                                                              style="width: 22px; float: right; height: 21px;"></a>
                                    </td>
                                    <td style="width: 25px;">&nbsp;</td>
                                    <td style="width: 25px;">&nbsp;</td>
                                    <td dir="ltr">
                                        <table cellspacing="0" cellpadding="0" border="0" align="left">
                                            <tbody>
                                            <tr>
                                                <td style="width: 115px;">
                                                    <div onclick="javascript:window.location.href='/search.html?Q='+ encodeURIComponent(document.getElementById('SiteSearch').value);"
                                                         style="    text-align: center;
    background-color: #4578b4;
    color: #ffffff;
    font-size: 8pt;
    height: 23px;
    padding-left: 2px;
    padding-right: 2pt;
    border-radius: 8px;
    cursor: pointer;
    border: 1px solid #fba52c;">&nbsp;<span style="font-size:9px;">حساب کاربری </span>&nbsp;
                                                    </div>
                                                </td>
                                                <td style="width: 10px;">&nbsp;</td>
                                                <td style="width: 30px; text-align: center;"><a
                                                            href="#/search.html"><img alt=""
                                                                                      src="{{ asset('images/frontend_images/home/magnifying-glass.png')}}"
                                                                                      style="width: 22px; height: 22px;"></a>
                                                </td>
                                                <td style="width: 30px; text-align: center;">&nbsp;</td>
                                                <td id="instlog" style="width: 30px; text-align: center;"><img alt=""
                                                                                                               src="{{ asset('images/frontend_images/home/instagram-logo.png')}}"
                                                                                                               style="width: 22px; height: 22px;">
                                                </td>
                                                <td style="width: 30px; text-align: center;"><a href="#"><img alt=""
                                                                                                              id="teleglog"
                                                                                                              src="{{ asset('images/frontend_images/home/telegramlogo1.png')}}"
                                                                                                              style="width: 22px; height: 22px;"></a>
                                                </td>
                                                <td style="width: 30px; text-align: center;"><img alt="" id="twitlog"
                                                                                                  src="{{ asset('images/frontend_images/home/twitter-logo-button.png')}}"
                                                                                                  style="width: 22px; height: 22px;">
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>


                        </div>
                        <div id="ctl24_iFooterPanel" class="SBFooterPanels"
                             style="text-align:left;border-radius:0px 0px 0px 0px;">

                            <span id="ctl24_iFooterTextToView"></span>
                        </div>
                    </div>
                    <div id="menush" class="SBContainerPanels"
                         style="border-width: 0px; border-style: none; width: 100%; box-shadow: rgb(119, 119, 119) 0px 0px 0px; margin: 0px 0px 5px; background-repeat: repeat-x; border-radius: 0px; height: 60px; background-position: left top; top: 36px; position: inherit;">
                        <div id="ctl25_iHeaderPanel" class="SBHeaderPanels"
                             style="text-align:left;padding-top:0px;padding-left:0px;padding-right:0px;border-radius:0px 0px 0px 0px;">

                            <span id="ctl25_iHeaderTextToView" class="aspNetDisabled"></span>

                        </div>
                        <div id="ctl25_iContentPanel" class="SBContentPanels"
                             style="padding-top:0px;padding-left:0px;padding-right:0px;padding-bottom:3px;">

                            <div class="GeneratedResponsiveMainMenu"><label class="show-menu" for="show-menu"><img
                                            src="{{ asset('images/frontend_images/home/responsive-menu-icon.png')}}" style="height:30px"
                                            onclick='$(this).parent().parent().find("ul:first").toggle()'></label>
                                <input
                                        id="show-menu" role="button" type="checkbox">
                                <ul style="padding:0px;background-color:transparent;margin-right:0px;margin-top:0px;"
                                    id="RootMenu">
                                    <li style="background-color:transparent;Border-Style:none;"><a href="#/"
                                                                                                   style="width:300px;height:65px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;"><img
                                                    src="{{ asset('images/frontend_images/home/ia.png')}}" style="height:60px"></a></li>
                                    <li style="background-color:transparent;Border-Style:none;"><a class="HasSubMenu"
                                                                                                   style="width:100px;height:65px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">درباره
                                            انجمن <i class="fa fa-sort-desc" aria-hidden="true"></i></a>
                                        <ul style="padding:0px;background-color:transparent;margin-right:0px;margin-top:0px;"
                                            class="hiddenMenu" id="MenuRootMenu61026">
                                            <li style="background-color:#e6e6e6;Border-Style:none;"><a
                                                        href="#/ForumHistory.html"
                                                        style="width:100px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:left;;display: table-cell;vertical-align: middle;">تاریخچه
                                                    انجمن</a></li>
                                            <li style="background-color:#e6e6e6;Border-Style:none;"><a
                                                        href="#/Forumgoals.html"
                                                        style="width:100px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">اهداف
                                                    انجمن</a></li>
                                            <li style="background-color:#e6e6e6;Border-Style:none;"><a
                                                        href="#/BoardFunctions.html"
                                                        style="width:100px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">وظایف
                                                    انجمن</a></li>
                                            <li style="background-color:#e6e6e6;Border-Style:none;"><a
                                                        href="#/BoardofDirectors.html"
                                                        style="width:100px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">هیئت
                                                    مدیره </a></li>
                                            <li style="background-color:#e6e6e6;Border-Style:none;"><a
                                                        href="#/BoardSecretary.html"
                                                        style="width:100px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">دبیر
                                                    انجمن</a></li>
                                            <li style="background-color:#e6e6e6;Border-Style:none;"><a
                                                        href="#/Statute.html"
                                                        style="width:100px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">اساسنامه</a>
                                            </li>
                                            <li style="background-color:#e6e6e6;Border-Style:none;"><a
                                                        href="#/Honors.html"
                                                        style="width:100px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">افتخارات</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li style="background-color:transparent;Border-Style:none;"><a class="HasSubMenu"
                                                                                                   style="width:100px;height:65px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">اعضای
                                            انجمن <i class="fa fa-sort-desc" aria-hidden="true"></i></a>
                                        <ul style="padding:0px;background-color:transparent;margin-right:0px;margin-top:0px;"
                                            class="hiddenMenu" id="MenuRootMenu61027">
                                            <li style="background-color:#e6e6e6;Border-Style:none;"><a
                                                        href="#/Termsandconditions.html"
                                                        style="width:135px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">شرایط
                                                    عضویت</a></li>
                                            <li style="background-color:#e6e6e6;Border-Style:none;"><a
                                                        href="#/Howtojoin.html"
                                                        style="width:135px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">نحوه
                                                    عضویت در انجمن</a></li>
                                            <li style="background-color:#e6e6e6;Border-Style:none;"><a
                                                        href="#/membershiprequest.html"
                                                        style="width:135px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">درخواست
                                                    عضویت</a></li>
                                            <li style="background-color:#222222;Border-Style:none;"><a
                                                        href="#/Forummembers.html"
                                                        style="width:135px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">اعضای
                                                    انجمن</a></li>
                                        </ul>
                                    </li>
                                    <li style="background-color:transparent;Border-Style:none;"><a class="HasSubMenu"
                                                                                                   style="width:60px;height:65px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">اخبار
                                            <i class="fa fa-sort-desc" aria-hidden="true"></i></a>
                                        <ul style="padding:0px;background-color:transparent;margin-right:0px;margin-top:0px;"
                                            class="hiddenMenu" id="MenuRootMenu61029">
                                            <li style="background-color:#222;Border-Style:none;"><a
                                                        href="#/ForumNews.html"
                                                        style="width:100px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">اخبار
                                                    انجمن </a></li>
                                            <li style="background-color:#222;Border-Style:none;"><a
                                                        href="#/specialnews.html"
                                                        style="width:100px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">اخبار
                                                    ویژه</a></li>
                                            <li style="background-color:#222;Border-Style:none;"><a
                                                        href="#/TheInternational.html"
                                                        style="width:100px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">بین
                                                    الملل</a></li>
                                            <li style="background-color:#222;Border-Style:none;"><a
                                                        href="#/Economic.html"
                                                        style="width:100px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">اقتصادی </a>
                                            </li>
                                            <li style="background-color:#222;Border-Style:none;"><a
                                                        href="#/Academic.html"
                                                        style="width:100px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">علمی</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li style="background-color:transparent;Border-Style:none;"><a
                                                style="width:80px;height:65px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">بین
                                            الملل</a></li>
                                    <li style="background-color:transparent;Border-Style:none;"><a class="HasSubMenu"
                                                                                                   style="width:70px;height:65px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">آمار
                                            <i class="fa fa-sort-desc" aria-hidden="true"></i></a>
                                        <ul style="padding:0px;background-color:transparent;margin-right:0px;margin-top:0px;"
                                            class="hiddenMenu" id="MenuRootMenu61030">
                                            <li style="background-color:#222;Border-Style:none;"><a href="#"
                                                                                                    style="width:100px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">آمار
                                                    ثبت سفارش</a></li>
                                            <li style="background-color:#222;Border-Style:none;"><a href="#"
                                                                                                    style="width:100px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">آمار
                                                    واردات</a></li>
                                            <li style="background-color:#222;Border-Style:none;"><a href="#"
                                                                                                    style="width:100px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">موجودی
                                                    مخازن</a></li>
                                            <li style="background-color:#222;Border-Style:none;"><a href="#"
                                                                                                    style="width:100px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">آمار
                                                    تولید و توضیع </a></li>
                                            <li style="background-color:#222;Border-Style:none;"><a href="#"
                                                                                                    style="width:100px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">بازار
                                                    جهانی</a></li>
                                            <li style="background-color:#222;Border-Style:none;"><a href="#"
                                                                                                    style="width:100px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">آمار
                                                    حمل</a></li>
                                            <li style="background-color:#222;Border-Style:none;"><a href="#"
                                                                                                    style="width:100px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">آمار
                                                    تحلیلی</a></li>
                                        </ul>
                                    </li>
                                    <li style="background-color:transparent;Border-Style:none;"><a class="HasSubMenu"
                                                                                                   style="width:70px;height:65px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">گالری
                                            <i class="fa fa-sort-desc" aria-hidden="true"></i></a>
                                        <ul style="padding:0px;background-color:transparent;margin-right:0px;margin-top:0px;"
                                            class="hiddenMenu" id="MenuRootMenu61031">
                                            <li style="background-color:#222222;Border-Style:none;"><a
                                                        href="#/reportimage.html"
                                                        style="width:100px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">گزارش
                                                    تصویری</a></li>
                                            <li style="background-color:#222222;Border-Style:none;"><a
                                                        href="#/Videoreport.html"
                                                        style="width:100px;height:35px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">گزارش
                                                    ویدیوئی</a></li>
                                        </ul>
                                    </li>
                                    <li style="background-color:transparent;Border-Style:none;"><a href="#/mahname.html"
                                                                                                   style="width:100px;height:65px;font-family:Arial;color:#fff;font-size:9pt;text-align:center;;display: table-cell;vertical-align: middle;">ماهنامه
                                            آفتابگردان</a></li>
                                </ul>
                            </div>
                        </div>
                        <div id="ctl25_iFooterPanel" class="SBFooterPanels"
                             style="text-align:left;border-radius:0px 0px 0px 0px;">
                            <span id="ctl25_iFooterTextToView"></span>
                        </div>
                    </div>
                </div>