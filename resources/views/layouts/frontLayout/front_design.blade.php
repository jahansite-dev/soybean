<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- Powered by SiteBike! CMS Plus 2.6, http://www.sitebike.ir -->
<html id="HTMLTag" xmlns="http://www.w3.org/1999/xhtml" class="direction" xml:lang="fa-ir" lang="fa-ir">
<head id="Head1"><title>
        انجمن صنفی صنایع روغن نباتی ایران
    </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta id="viewport" name="viewport"
          content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi">
    <meta id="TelegramText" property="og:title" content="انجمن صنفی صنایع روغن نباتی ایران">
    <meta id="TelegramImage" property="og:image" content="/files/ivoia-com/Images/icon/fav.png">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <style>
        .SBContainerPanels {
            margin-left: auto !important;
            margin-right: auto !important;
        }


        .ResponsiveCenter {
            margin-left: auto !important;
            margin-right: auto !important;
        }

        .DisplayBlock {
            display: inline-block;
            vertical-align: top;
        }

        #LastSideZoneBackgroundTD, #FirstSideZoneBackgroundTD {
            /*  max-width:120px;*/
        }

        #TemplateHeader2ItemParent {
            /*max-width:600px;*/
        }

        #OuterInlineContentsControl {
            text-align: center;
        }

        .initialTextAlign {
            text-align: initial;
        }

        .clearAlign {
            clear: both;
        }
    </style>
{{--css--}}
    <link type="text/css" rel="stylesheet" href="{{ asset('css/frontend_css/jssorSlider.css')}}">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/frontend_css/HitCounter.css')}}">
    <link type="image/x-icon" rel="shortcut icon" href="{{ asset('#/files/ivoia-com/Images/icon/fav.png')}}">
    <link id="PageCSSLink" rel="stylesheet" type="text/css" href="{{ asset('css/frontend_css/fontawesome-free-5.11.2-web/css/all.min.css')}}">
    <link id="PageCSSLink" rel="stylesheet" type="text/css" href="{{ asset('css/frontend_css/fontawesome-free-5.11.2-web/css/brands.min.css')}}">
    <link id="PageCSSLink" rel="stylesheet" type="text/css" href="{{ asset('css/frontend_css/fontawesome-free-5.11.2-web/css/fontawesome.min.css')}}">
    <link id="PageCSSLink" rel="stylesheet" type="text/css" href="{{ asset('css/frontend_css/fontawesome-free-5.11.2-web/css/regular.min.css')}}">
    <link id="PageCSSLink" rel="stylesheet" type="text/css" href="{{ asset('css/frontend_css/fontawesome-free-5.11.2-web/css/solid.min.css')}}">
    <link id="PageCSSLink" rel="stylesheet" type="text/css" href="{{ asset('css/frontend_css/fontawesome-free-5.11.2-web/css/svg-with-js.min.css')}}">
    <link id="PageCSSLink" rel="stylesheet" type="text/css" href="{{ asset('css/frontend_css/fontawesome-free-5.11.2-web/css/v4-shims.min.css')}}">
    <link href="{{ asset('css/frontend_css/CssGeneratorHandler.css')}}" rel="stylesheet" type="text/css">
    <link id="PageCSSLink" rel="stylesheet" type="text/css" href="{{ asset('css/frontend_css/PageCssGenerator.css')}}">
    <link href="{{ asset('css/frontend_css/slick.css')}}" rel="stylesheet">
    <link href="{{ asset('css/frontent_css/slick-theme.css')}}" rel="stylesheet">
{{--javascript--}}
    <script src="{{ asset('js/frontend_js/jquery-2.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/frontend_js/slick.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/frontend_js/WebResource.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/frontend_js/scripts.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/frontend_js/jssor.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/frontend_js/TabsRotate.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/frontend_js/ScriptResource_002.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/frontend_js/ScriptResource.js')}}" type="text/javascript"></script>
    <script src="{{('js/frontend_js/js.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/frontend_js/fontawesome-free-5.11.2-web/js/all.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/frontend_js/fontawesome-free-5.11.2-web/js/brand.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/frontend_js/fontawesome-free-5.11.2-web/js/conflict-detection.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/frontend_js/fontawesome-free-5.11.2-web/js/fontawesome.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/frontend_js/fontawesome-free-5.11.2-web/js/regular.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/frontend_js/fontawesome-free-5.11.2-web/js/solid.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/frontend_js/fontawesome-free-5.11.2-web/js/v4-shims.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/frontend_js/jquery.js')}}" type="text/javascript"></script>
    <style type="text/css">.jqstooltip {
            position: absolute;
            left: 0px;
            top: 0px;
            visibility: hidden;
            background: rgb(0, 0, 0) transparent;
            background-color: rgba(0, 0, 0, 0.6);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);
            -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";
            color: white;
            font: 10px arial, san serif;
            text-align: left;
            white-space: nowrap;
            padding: 5px;
            border: 1px solid white;
            z-index: 10000;
        }

        .jqsfield {
            color: white;
            font: 10px arial, san serif;
            text-align: left;
        }</style>
</head>
<body>
<script type="text/javascript">
    $('.tiptip').tipTip({maxWidth: 'auto', edgeOffset: 3})
    if ($(window).width() <= 700) {
        $('#RootMenu li:first').hide();
        $('<a href="/" ><img  style="z-index: 40000;position: fixed;width: 166px;top: 36px;    right: 46px;" src="/files/ivoia-com//Images/TemplateImages/Logo-transparent2.png" id="AfterLoginMenu" ></a>').insertAfter(".show-menu");
    }


    $(window).scroll(function () {
        var height = $(window).scrollTop();

        if (height > 100) {

            $('#HeadThin').fadeOut();
            $('#menush').css("top", "0px");
            $('#AfterLoginMenu').css("top", "0px");

            $('#menush').css("position", "fixed");

        } else {

            $('#HeadThin').fadeIn();
            $('#menush').css("top", "36px");
            $('#menush').css("position", "inherit");
            $('#AfterLoginMenu').css("top", "36px");

        }
    });
</script>
<div id="ctl31_iFooterPanel" class="SBFooterPanels"
     style="text-align:left;border-radius:0px 0px 0px 0px;">

    <span id="ctl31_iFooterTextToView"></span>

</div>
<div id="ctl32_iViewMode" class="SBContainerPanels"
     style="border-color:;border-width:0px;border-style:None;-webkit-box-shadow:0px 0px 0px #777;-webkit-border-radius:0px;-moz-border-radius:0px;box-shadow:0px 0px 0px #777;margin-left:0px;margin-bottom:0px;border-radius:0px;margin-right:0px;margin-top:0px;">


    <div id="ctl32_iContentPanel"
         style="padding-top:0px;padding-left:0px;padding-right:0px;padding-bottom:0px;">

        <style type="text/css">
            #ctl15_iContentPanel {
                line-height: 10px;
            }

            rect {
                border-radius: 0 0 55px 0;
            }

            /***********************************/
            .SelectedNewsPagerItem {
                font-size: 19px !important;
                margin: 2px !important;
                border: 1px solid rgb(217, 217, 217);
                padding: 3px;
                width: 27px;
                display: inline-block;
                height: 27px;
                background-color: rgb(168, 26, 1);
                border-radius: 36px;
                color: rgb(253, 253, 253);
                margin-top: 24px !important;
                box-shadow: 2px 2px 2px silver;
                display: inline-block;
                border: rgb(168, 26, 1);
                /*
                font-size: 19px !important;
                margin: 2px !important;
                border: 1px solid rgb(217, 217, 217);
                padding: 3px;
                width: 20px;
                display:inline-block;
                height: 20px;
                background-color: rgb(168, 26, 1);
                border-radius: 13px;
                color: rgb(253, 253, 253);*/
            }


            #HeadObjId {
                background-size: 330px;
            }

            #topmenu {
                border-radius: 4px 4px 0px 0px !important;
            }

            .ui-tabs .ui-tabs-nav {
                height: 36px;
            !important;
                border: none;
                border-radius: 3px 3px 0px 0px;
            }

            .ui-widget-header { /*background: #333 url(/files/fava24-ir/Image/TemplateImage/BoxBgHead1.png) 50% 50% repeat !important;*/
            }

            .ui-tabs .ui-tabs-nav li {
                height: 30px !important;
            }

            .ui-widget-content {
                background: none !important;
            }

            .ui-tabs .ui-tabs-panel {
                padding: 0px !important;
            }


            #featured .ui-tabs-selected h6 {
                color: #fff !important;
            }

            #featured {
                background: none;
                overflow: hidden;
                border: none;
                width: 99%;
                height: 230px;
            }

            .SliderNewsThumbnailImgs {
                float: right !important;
                width: 62px !important;
                height: 49px !important;
                border: 1px solid #e0dfdf;
                padding: 3px;
                margin: 6px;

            }

            #featured .ui-widget-header .ui-state-default {
                overflow: hidden !important;
                background-color: white;
                background-image: none;
                border-bottom: 1px solid silver !important;
                border-right: 1px solid silver !important;
            }

            #featured .ui-widget-header:last {
                background-color: white;
                background-image: none;
                border-bottom: none !important;

            }


            #featured .ui-state-hover {
                background-image: url(/files/ime-co-ir/images/backgrounds/vert_tab_even_line.jpg) !important;

            }

            #featured .ui-state-active {
                background-image: url(/files/ime-co-ir/images/backgrounds/vert_tab_even_line.jpg) !important;
                background-repeat: repeat-x;


            }


            #featured .info h6 {

                font-size: 16px !important;
                font-weight: bold;
                font-family: IRANSans;
                margin-bottom: 10px;
                margin-top: 10px;
                color: #104d8d
            }


            #featured .info p, #RotatorSliderReadMore {

                font-size: 12px;

                font-family: IRANSans;
            }

            #RotatorSliderReadMore {

                text-align: left;

            }

            #featured .info .NewsImgs {
                float: right;
                width: 230px;
                margin: 7px;
                padding: 3px;
                border: 1px solid silver;
                border-radius: 3px;

            }

            #featured .rotatorcontents {
                /*width:895px;*/
                /*width:60%;*/

            }

            #featured .featuredContentsHolder {
                width: 72%;
            }

            #featured .ui-tabs-panel .info {
                height: 100%;
                position: relative;
                top: -5px;
            }

            #featured li a {

                padding: 0px !important;
            }


            #featured .ui-widget-header {
                border: none !important;
                background: none !important;
                border: none;
            }

            #featured a {
                border: none;
            }

            #featured h6 {

                padding: 0px;
                margin: 0px;
                font-size: 11px;
                line-height: 1.6;
                font-weight: normal;
                overflow: hidden;

                padding: 10px 0 0 3px;

            }


            #featured ul.ui-tabs-nav {
                float: left;
                list-style: none;
                padding: 0;
                margin-top: 3px;
                /*width: 306px;*/
                /*width:40%;*/
                margin-left: -4px;

            }

            #featured .featuredULHolder {
                width: 25%;
                float: left;
            }

            #featured ul.ui-tabs-nav li {

                padding: 1px 0;
                font-size: 8px;
                border: none;
                margin: 0px !important;
                height: 67px !important;
                vertical-align: bottom;
                width: 98%;
            }

            #featured ul.ui-tabs-nav li span {
                font-size: 13px;

                line-height: 12px;
            }

            #featured li.ui-tabs-nav-item a {
                white-space: normal;

                display: block;
                height: 100%;
                text-decoration: none;
                color: #6e3b21;
                outline: none;
                /*width: 291px;*/
                width: 100%;
                color: #0a0a0a;
                font-size: 12px;
            }

            #featured .ui-tabs-panel {
                /*float: right;*/
                padding-right: 6px !important;
                padding-top: 2px !important;
                height: 183px;
                margin-TOP: 7px;
                padding-left: 7px !important;
            }

            #featured .ui-tabs-panel .info {
                height: 100%;
            }

            #featured .ui-tabs-panel .info a.hideshow {
                position: absolute;
                font-size: 11px;
                font-family: IRANSans;
                color: #f0f0f0;
                right: 10px;
                top: -20px;
                margin: 0;
                outline: none;
            }

            #featured .info h2 {
                padding: 5px;
                margin: 0;
                font-weight: normal;
                overflow: hidden;

            }


            #featured .info p {
                margin: 4px 3px 4px 0px;
                color: #333;
                text-align: justify;
                direction: rtl;
            }

            #featured .info a {
                text-decoration: none;

            }

            #featured .ui-tabs-hide {
                display: none;
            }

            .rotatorcontents {
                /*width: 370px;*/
                text-align: right;
            }


            #featured .ui-state-hover {
                margin: 0px !important;
                padding: 0px;
            }

            #featured .ui-widget-header .ui-state-default {
                margin: 2px !important;
                border-radius: 20px 5px 5px 5px;
                border: none;

            }

            #tazehtarin li {
                line-height: 25px;
            }

            #tazehtarin ul {
                padding-right: 25px;
            }

            /* -----------END SLIDER ------------------*/


        </style>


    </div>


</div>
<script>

    function GeneratePieChart(cnt, data, DoNotAddPercent) {
        var percentStr = "٪";
        var LastPercentStr = "";
        if (DoNotAddPercent) {
            percentStr = "";
            LastPercentStr = "٪";

        }
        Highcharts.chart(cnt, {
            chart: {
                margin: [0, 0, 0, 0],
                spacingTop: 0,
                spacingBottom: 0,
                spacingLeft: 0,
                spacingRight: 0,
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie', options3d: {
                    enabled: true,
                    alpha: 45
                },
                rtl: true
            }, credits: {
                enabled: false
            },
            title: {
                text: ''
            },

            tooltip: {
                shared: true,
                useHTML: true,
                formatter: function () {
                    return '<div style="direction:rtl; text-align:center; font-size:16px;font-family:IRANSans">' + this.key + '</div><table><tr><td style="text-align: center;color:' + this.series.color + ';width:100px;"><b style="margin:4px;">' + Highcharts.numberFormat(this.y, 0, '.', ',') + '</b></td></tr></table>';
                }
            },
            plotOptions: {
                pie: {
                    size: '85%',
                    innerSize: 60,
                    depth: 45,
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            return '<div style="direction:rtl; text-align:center; font-size:16px;font-family:IRANSans">' + percentStr + this.key + LastPercentStr + '</div><table><tr><td style="text-align: center;color:' + this.series.color + ';width:100px;"><b style="margin:4px;">' + this.percentage.toFixed(1) + '</b></td></tr></table>';
                        },

//format: '<b>{point.name}</b>: {point.percentage:.1f} درصد',

                        style: {
                            fontFamily: '\'IRANSans\'',
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                name: '',
                colorByPoint: true,
                data: data
            }]
        });
    }


    $(document).ready(function () {

            MEWService.GetExchageRateGrid(false, "MEblR4bFBvMXcyRk09", 0, 10, false, onChartDataReceived, null, null);
        }
    )

    function onChartDataReceived(sender) {

        PieChartData = [];
        reza = sender;
        for (chartCounter = 0; chartCounter < 4; chartCounter++) {
            PieChartData.push({
                name: sender.Currencies[chartCounter].CurrencyName + " ",
                y: sender.Currencies[chartCounter].CurrencyValues[0] * 1
            });

        }
        GeneratePieChart('PieChartData', PieChartData, true)


        var chart = new Highcharts.Chart({
            chart: {
                renderTo: 'BarChartData', rtl: true,
                type: 'column',

                options3d: {
                    enabled: true,
                    alpha: 15,
                    beta: 15,
                    depth: 50,
                    viewDistance: 25
                }
            }, credits: {enabled: false},
            title: {text: ''},

            yAxis: {
                allowDecimals: false,
                title: {
                    text: ''
                },
                visible: false
            },
            plotOptions: {
                column: {
                    depth: 25
                }
            },
            series: [{
                name: sender.Currencies[0].CurrencyName,
                data: sender.Currencies[0].CurrencyValues
            },
                {
                    name: sender.Currencies[1].CurrencyName,
                    data: sender.Currencies[1].CurrencyValues
                },
                {
                    name: sender.Currencies[2].CurrencyName,
                    data: sender.Currencies[2].CurrencyValues
                },
                {
                    name: sender.Currencies[3].CurrencyName,
                    data: sender.Currencies[3].CurrencyValues
                },
                {
                    name: sender.Currencies[4].CurrencyName,
                    data: sender.Currencies[4].CurrencyValues
                },
                {
                    name: sender.Currencies[5].CurrencyName,
                    data: sender.Currencies[5].CurrencyValues
                }]
        });

    }


</script>
<script type="text/javascript">
    //<![CDATA[


    var LastUpdateDateFromServerblR4bFBvMXcyRk09;

    function CheckLastEditedDatablR4bFBvMXcyRk09(sender) {

        if (sender != LastUpdateDateFromServerblR4bFBvMXcyRk09) {
            LastUpdateDateFromServerblR4bFBvMXcyRk09 = sender;
            MoneyExchangeUpdateControlData('MEblR4bFBvMXcyRk09', false, 0, 20, false);

        }
        window.setTimeout(
            function () {
                MEWService.GetIntLastUpdate(CheckLastEditedDatablR4bFBvMXcyRk09, onFailure, userContext);

            }
            , 10000);

//window.setTimeout(tstTimer, 10000);
    }


    function AutoblR4bFBvMXcyRk09LoadExchangeData() {

        MEWService.GetIntLastUpdate(CheckLastEditedDatablR4bFBvMXcyRk09, onFailure, userContext);
    }

    $(document).ready(function () {
        AutoblR4bFBvMXcyRk09LoadExchangeData();
    });

    slidercontentslider_dHUwbjZ5c01ScjA9_slider_init = function () {


        var slidercontentslider_dHUwbjZ5c01ScjA9_options = {
            $AutoPlay: true,
            $SlideDuration: 800,
            $SlideEasing: $Jease$.$OutQuint,
            $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$
            },
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
            },
            $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
            }
        };

        var slidercontentslider_dHUwbjZ5c01ScjA9_slider = new $JssorSlider$('slidercontentslider_dHUwbjZ5c01ScjA9', slidercontentslider_dHUwbjZ5c01ScjA9_options);

        //responsive code begin
        //you can remove responsive code if you don't want the slider scales while window resizing
        function ScaleSlider() {

            var refSize = slidercontentslider_dHUwbjZ5c01ScjA9_slider.$Elmt.parentNode.clientWidth;
            if (refSize) {
                refSize = Math.min(refSize, 1920);
                slidercontentslider_dHUwbjZ5c01ScjA9_slider.$ScaleWidth(refSize);
            } else {
                window.setTimeout(ScaleSlider, 30);
            }
        }

        ScaleSlider();
        $Jssor$.$AddEvent(window, 'load', ScaleSlider);
        $Jssor$.$AddEvent(window, 'resize', ScaleSlider);
        $Jssor$.$AddEvent(window, 'orientationchange', ScaleSlider);

    };
    // $(document).ready(function () {
    //     slidercontentslider_dHUwbjZ5c01ScjA9_slider_init();
    // })


    $(document).ready(function () {
        $('#featured').tabs({show: {effect: 'fade', duration: 1000}}).tabs('rotate', 8000, true);
    });
    //]]>
</script>
<script>
    $('#Slicker').slick({
        infinite: true, rtl: true,
        slidesToShow: 10,
        slidesToScroll: 3,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: false,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            }
        ]


    });

</script>
<script>
    $(".NewsLayout7 h2").each(function (i) {
        len = $(this).text().length;
        if (len > 55) {
            $(this).text($(this).text().substr(0, 45) + '...');
        }
    });

    $(".NewsLayout7SubNewsDiv a").each(function (i) {
        len = $(this).text().length;
        if (len > 41) {
            $(this).text($(this).text().substr(0, 43) + '...');
        }
    });

    $('#akharinakhbar .newsImgsTD p').hide();
    $("#akharinakhbar  h2").each(function (i) {
        len = $(this).text().length;
        if (len > 55) {
            $(this).text($(this).text().substr(0, 60) + '...');
        }
    });
</script>
<script type="text/javascript">
    //<![CDATA[
    $(function () {
        try {
            $('.CMSPlusContentPageTabs').tabs();
        } catch (e) {
        }
    });
    $(document).ready(function () {
        try {
            LoadMeAfterCallBack();
        } catch (e) {
        }
    });


    $(document).ready(function () {
        $('.ui-tabs').css('position', 'initial');
    });


    function onFailure(sender, e) {/*alert('Problem retrieving XML data');*/
    }

    function userContext(sender) {/*alert(sender);*/
    }

    function ShowModalMessageHW(WindowHeader, msg, height, width) {
        $('#ModalMsgDlgDiv').attr('title', WindowHeader);
        $('#MessageLabel').html(msg);
        $('#ModalMsgDlgDiv').dialog({
            height: height,
            width: width,
            resizable: false,
            modal: true,
            buttons: {
                قبول: function () {
                    $('#ModalMsgDlgDiv').dialog('close');
                }
            }
        });
    }

    function ShowModalMessage(WindowHeader, msg) {
        ShowModalMessageHW(WindowHeader, msg, 210, 400);
    }

    function ShowModalWaitMessage(WindowHeader, msg, height, width) {
        $('#WaitDlg').attr('title', WindowHeader);
        $('#WaitDlgLbl').html(msg);
        $('#WaitDlg').dialog({height: height, width: width, resizable: false, modal: true});
    }

    function CloseModalWaitMessage() {
        $('#WaitDlg').dialog('close');
    }

    Sys.Application.add_init(function () {
        $create(Sys.UI._UpdateProgress, {
            "associatedUpdatePanelId": "UpdatePanel1",
            "displayAfter": 1,
            "dynamicLayout": true
        }, null, null, $get("updProgress"));
    });
    //]]>
</script>
<style>
    #MenuContainer {
        text-align: center;
    }

    .NewsLayout7SubNewsDiv li {
        list-style-type: none;
        border-bottom: 1px solid silver;
        width: 100%;
        padding-top: 4px;
        padding-bottom: 4px;
    }


    .NewsLayout7SubNewsDiv * {
        text-align: justify;

    }

    .NewsLayout7SubNewsDiv a {
        color: #104d8d;
        font-size: 8.5pt !important;
    }

    .NewsLayout7MainNewsDiv {
        min-height: 394px;
        padding: 12px;
    }

    .NewsLayout7MainNewsDiv p {
        text-align: justify;
    }

    #IMMC1, #IMMC2, #IMMC3, #IMMC4 {
        width: 304px;
    }

    #akharinakhbarmob {
        display: none;
    }

    @media (max-width: 900px) {

        #akharinakhbarmob {
            display: block;
        }

        #topholdnews {
            display: none;
        }
    }

    @media (min-width: 946px) and (max-width: 1228px) {
        #IMMC4 {
            width: 99% !important;
        }

        #IMMC4 .SBContainerPanels {
            display: inline-block;
            width: 300px !important;
            vertical-align: top;
        }
    }
</style>
<style>
    .mySlides {
        display: none
    }

    .w3-left, .w3-right, .w3-badge {
        cursor: pointer
    }

    .w3-badge {
        height: 13px;
        width: 13px;
        padding: 0
    }
</style>
<style>
    #ctl15_iContentPanel {
        line-height: 10px;
    }

    rect {
        border-radius: 0 0 55px 0;
    }

    /***********************************/
    .SelectedNewsPagerItem {
        font-size: 19px !important;
        margin: 2px !important;
        border: 1px solid rgb(217, 217, 217);
        padding: 3px;
        width: 27px;
        display: inline-block;
        height: 27px;
        background-color: rgb(168, 26, 1);
        border-radius: 36px;
        color: rgb(253, 253, 253);
        margin-top: 24px !important;
        box-shadow: 2px 2px 2px silver;
        display: inline-block;
        border: rgb(168, 26, 1);
        /*
        font-size: 19px !important;
        margin: 2px !important;
        border: 1px solid rgb(217, 217, 217);
        padding: 3px;
        width: 20px;
        display:inline-block;
        height: 20px;
        background-color: rgb(168, 26, 1);
        border-radius: 13px;
        color: rgb(253, 253, 253);*/
    }


    #HeadObjId {
        background-size: 330px;
    }

    #topmenu {
        border-radius: 4px 4px 0px 0px !important;
    }

    .ui-tabs .ui-tabs-nav {
        height: 36px;
    !important;
        border: none;
        border-radius: 3px 3px 0px 0px;
    }

    .ui-widget-header { /*background: #333 url(/files/fava24-ir/Image/TemplateImage/BoxBgHead1.png) 50% 50% repeat !important;*/
    }

    .ui-tabs .ui-tabs-nav li {
        height: 30px !important;
    }

    .ui-widget-content {
        background: none !important;
    }

    .ui-tabs .ui-tabs-panel {
        padding: 0px !important;
    }


    #featured .ui-tabs-selected h6 {
        color: #fff !important;
    }

    #featured {
        background: none;
        overflow: hidden;
        border: none;
        width: 99%;
        height: 230px;
    }

    .SliderNewsThumbnailImgs {
        float: right !important;
        width: 62px !important;
        height: 49px !important;
        border: 1px solid #e0dfdf;
        padding: 3px;
        margin: 6px;

    }

    #featured .ui-widget-header .ui-state-default {
        overflow: hidden !important;
        background-color: white;
        background-image: none;
        border-bottom: 1px solid silver !important;
        border-right: 1px solid silver !important;
    }

    #featured .ui-widget-header:last {
        background-color: white;
        background-image: none;
        border-bottom: none !important;

    }


    #featured .ui-state-hover {
        background-image: url(/files/ime-co-ir/images/backgrounds/vert_tab_even_line.jpg) !important;

    }

    #featured .ui-state-active {
        background-image: url(/files/ime-co-ir/images/backgrounds/vert_tab_even_line.jpg) !important;
        background-repeat: repeat-x;


    }


    #featured .info h6 {

        font-size: 16px !important;
        font-weight: bold;
        font-family: IRANSans;
        margin-bottom: 10px;
        margin-top: 10px;
        color: #104d8d
    }


    #featured .info p, #RotatorSliderReadMore {

        font-size: 12px;

        font-family: IRANSans;
    }

    #RotatorSliderReadMore {

        text-align: left;

    }

    #featured .info .NewsImgs {
        float: right;
        width: 230px;
        margin: 7px;
        padding: 3px;
        border: 1px solid silver;
        border-radius: 3px;

    }

    #featured .rotatorcontents {
        /*width:895px;*/
        /*width:60%;*/

    }

    #featured .featuredContentsHolder {
        width: 72%;
    }

    #featured .ui-tabs-panel .info {
        height: 100%;
        position: relative;
        top: -5px;
    }

    #featured li a {

        padding: 0px !important;
    }


    #featured .ui-widget-header {
        border: none !important;
        background: none !important;
        border: none;
    }

    #featured a {
        border: none;
    }

    #featured h6 {

        padding: 0px;
        margin: 0px;
        font-size: 11px;
        line-height: 1.6;
        font-weight: normal;
        overflow: hidden;

        padding: 10px 0 0 3px;

    }


    #featured ul.ui-tabs-nav {
        float: left;
        list-style: none;
        padding: 0;
        margin-top: 3px;
        /*width: 306px;*/
        /*width:40%;*/
        margin-left: -4px;

    }

    #featured .featuredULHolder {
        width: 25%;
        float: left;
    }

    #featured ul.ui-tabs-nav li {

        padding: 1px 0;
        font-size: 8px;
        border: none;
        margin: 0px !important;
        height: 67px !important;
        vertical-align: bottom;
        width: 98%;
    }

    #featured ul.ui-tabs-nav li span {
        font-size: 13px;

        line-height: 12px;
    }

    #featured li.ui-tabs-nav-item a {
        white-space: normal;

        display: block;
        height: 100%;
        text-decoration: none;
        color: #6e3b21;
        outline: none;
        /*width: 291px;*/
        width: 100%;
        color: #0a0a0a;
        font-size: 12px;
    }

    #featured .ui-tabs-panel {
        /*float: right;*/
        padding-right: 6px !important;
        padding-top: 2px !important;
        height: 183px;
        margin-TOP: 7px;
        padding-left: 7px !important;
    }

    #featured .ui-tabs-panel .info {
        height: 100%;
    }

    #featured .ui-tabs-panel .info a.hideshow {
        position: absolute;
        font-size: 11px;
        font-family: IRANSans;
        color: #f0f0f0;
        right: 10px;
        top: -20px;
        margin: 0;
        outline: none;
    }

    #featured .info h2 {
        padding: 5px;
        margin: 0;
        font-weight: normal;
        overflow: hidden;

    }


    #featured .info p {
        margin: 4px 3px 4px 0px;
        color: #333;
        text-align: justify;
        direction: rtl;
    }

    #featured .info a {
        text-decoration: none;

    }

    #featured .ui-tabs-hide {
        display: none;
    }

    .rotatorcontents {
        /*width: 370px;*/
        text-align: right;
    }


    #featured .ui-state-hover {
        margin: 0px !important;
        padding: 0px;
    }

    #featured .ui-widget-header .ui-state-default {
        margin: 2px !important;
        border-radius: 20px 5px 5px 5px;
        border: none;

    }

    #tazehtarin li {
        line-height: 25px;
    }

    #tazehtarin ul {
        padding-right: 25px;
    }

    /* -----------END SLIDER ------------------*/


</style>
<script>
    var slideIndex = 1;
    showDivs(slideIndex);

    function plusDivs(n) {
        showDivs(slideIndex += n);
    }

    function currentDiv(n) {
        showDivs(slideIndex = n);
    }

    function showDivs(n) {
        var i;
        var x = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("demo");
        if (n > x.length) {
            slideIndex = 1
        }
        if (n < 1) {
            slideIndex = x.length
        }
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" w3-white", "");
        }
        x[slideIndex - 1].style.display = "block";
        dots[slideIndex - 1].className += " w3-white";
    }
</script>
<script>

    function GeneratePieChart(cnt, data, DoNotAddPercent) {
        var percentStr = "٪";
        var LastPercentStr = "";
        if (DoNotAddPercent) {
            percentStr = "";
            LastPercentStr = "٪";

        }
        Highcharts.chart(cnt, {
            chart: {
                margin: [0, 0, 0, 0],
                spacingTop: 0,
                spacingBottom: 0,
                spacingLeft: 0,
                spacingRight: 0,
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie', options3d: {
                    enabled: true,
                    alpha: 45
                },
                rtl: true
            }, credits: {
                enabled: false
            },
            title: {
                text: ''
            },

            tooltip: {
                shared: true,
                useHTML: true,
                formatter: function () {
                    return '<div style="direction:rtl; text-align:center; font-size:16px;font-family:IRANSans">' + this.key + '</div><table><tr><td style="text-align: center;color:' + this.series.color + ';width:100px;"><b style="margin:4px;">' + Highcharts.numberFormat(this.y, 0, '.', ',') + '</b></td></tr></table>';
                }
            },
            plotOptions: {
                pie: {
                    size: '85%',
                    innerSize: 60,
                    depth: 45,
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        formatter: function () {
                            return '<div style="direction:rtl; text-align:center; font-size:16px;font-family:IRANSans">' + percentStr + this.key + LastPercentStr + '</div><table><tr><td style="text-align: center;color:' + this.series.color + ';width:100px;"><b style="margin:4px;">' + this.percentage.toFixed(1) + '</b></td></tr></table>';
                        },

//format: '<b>{point.name}</b>: {point.percentage:.1f} درصد',

                        style: {
                            fontFamily: '\'IRANSans\'',
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                name: '',
                colorByPoint: true,
                data: data
            }]
        });
    }


    $(document).ready(function () {

            MEWService.GetExchageRateGrid(false, "MEblR4bFBvMXcyRk09", 0, 10, false, onChartDataReceived, null, null);
        }
    )

    function onChartDataReceived(sender) {

        PieChartData = [];
        reza = sender;
        for (chartCounter = 0; chartCounter < 4; chartCounter++) {
            PieChartData.push({
                name: sender.Currencies[chartCounter].CurrencyName + " ",
                y: sender.Currencies[chartCounter].CurrencyValues[0] * 1
            });

        }
        GeneratePieChart('PieChartData', PieChartData, true)


        var chart = new Highcharts.Chart({
            chart: {
                renderTo: 'BarChartData', rtl: true,
                type: 'column',

                options3d: {
                    enabled: true,
                    alpha: 15,
                    beta: 15,
                    depth: 50,
                    viewDistance: 25
                }
            }, credits: {enabled: false},
            title: {text: ''},

            yAxis: {
                allowDecimals: false,
                title: {
                    text: ''
                },
                visible: false
            },
            plotOptions: {
                column: {
                    depth: 25
                }
            },
            series: [{
                name: sender.Currencies[0].CurrencyName,
                data: sender.Currencies[0].CurrencyValues
            },
                {
                    name: sender.Currencies[1].CurrencyName,
                    data: sender.Currencies[1].CurrencyValues
                },
                {
                    name: sender.Currencies[2].CurrencyName,
                    data: sender.Currencies[2].CurrencyValues
                },
                {
                    name: sender.Currencies[3].CurrencyName,
                    data: sender.Currencies[3].CurrencyValues
                },
                {
                    name: sender.Currencies[4].CurrencyName,
                    data: sender.Currencies[4].CurrencyValues
                },
                {
                    name: sender.Currencies[5].CurrencyName,
                    data: sender.Currencies[5].CurrencyValues
                }]
        });

    }


</script>    <script>
    $('#Slicker').slick({
        infinite: true, rtl: true,
        slidesToShow: 10,
        slidesToScroll: 3,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: false,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            }
        ]


    });

</script>
<script>
    $(".NewsLayout7 h2").each(function (i) {
        len = $(this).text().length;
        if (len > 55) {
            $(this).text($(this).text().substr(0, 45) + '...');
        }
    });

    $(".NewsLayout7SubNewsDiv a").each(function (i) {
        len = $(this).text().length;
        if (len > 41) {
            $(this).text($(this).text().substr(0, 43) + '...');
        }
    });

    $('#akharinakhbar .newsImgsTD p').hide();
    $("#akharinakhbar  h2").each(function (i) {
        len = $(this).text().length;
        if (len > 55) {
            $(this).text($(this).text().substr(0, 60) + '...');
        }
    });
</script>
<script type="text/javascript">
    //<![CDATA[
    $(function () {
        try {
            $('.CMSPlusContentPageTabs').tabs();
        } catch (e) {
        }
    });
    $(document).ready(function () {
        try {
            LoadMeAfterCallBack();
        } catch (e) {
        }
    });


    $(document).ready(function () {
        $('.ui-tabs').css('position', 'initial');
    });


    function onFailure(sender, e) {/*alert('Problem retrieving XML data');*/
    }

    function userContext(sender) {/*alert(sender);*/
    }

    function ShowModalMessageHW(WindowHeader, msg, height, width) {
        $('#ModalMsgDlgDiv').attr('title', WindowHeader);
        $('#MessageLabel').html(msg);
        $('#ModalMsgDlgDiv').dialog({
            height: height,
            width: width,
            resizable: false,
            modal: true,
            buttons: {
                قبول: function () {
                    $('#ModalMsgDlgDiv').dialog('close');
                }
            }
        });
    }

    function ShowModalMessage(WindowHeader, msg) {
        ShowModalMessageHW(WindowHeader, msg, 210, 400);
    }

    function ShowModalWaitMessage(WindowHeader, msg, height, width) {
        $('#WaitDlg').attr('title', WindowHeader);
        $('#WaitDlgLbl').html(msg);
        $('#WaitDlg').dialog({height: height, width: width, resizable: false, modal: true});
    }

    function CloseModalWaitMessage() {
        $('#WaitDlg').dialog('close');
    }

    Sys.Application.add_init(function () {
        $create(Sys.UI._UpdateProgress, {
            "associatedUpdatePanelId": "UpdatePanel1",
            "displayAfter": 1,
            "dynamicLayout": true
        }, null, null, $get("updProgress"));
    });
    //]]>
</script>

</form>
<div id="tiptip_holder" style="margin: 1971px 0px 0px 193px; display: none;" class="tip_bottom">
    <div id="tiptip_arrow" style="margin-right: 154.5px; margin-top: -12px;">
        <div id="tiptip_arrow_inner"></div>
    </div>
    <div id="tiptip_content">بازدید دبیر انجمن صنفی صنایع روغن نباتی از کارخانه نوش آذر<br>دوشنبه 23 مهر 1397 08:24 ق.ظ
    </div>
</div>
@include('layouts.frontLayout.index_header')

@yield('content')

@include('layouts.frontLayout.index_footer')
</body>
</html>